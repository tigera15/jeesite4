/***
 *    freestyle 公共js库
 ***/

var mFieldsToUpper=[];
(function(owner){
    owner.gridRowStatusAction=function (pvThis,pvsHint,pvGridId,pvsId,pvsStatus){
        var _this=pvThis;
        if (typeof pvsStatus=="undefined"){
            var lvsCurStatus=$('#'+pvsId+'_status').val();
            if (lvsCurStatus=="0"){
                pvsHint='你确认要停用这条数据吗？';
                pvsStatus="2";
            }
            else{
                pvsHint='你确认要启用这条数据吗？';
                pvsStatus="0";
            }
            if ($("#{0}_isModified".format(pvsId)).length==0){
                js.alert("忘记在datagrid的'id' column后面加上'isModified'列!");
            }
            $("#{0}_isModified".format(pvsId)).val("1");
        }
        js.confirm(/*'你确认要停用这条数据吗？'*/pvsHint,
            function(){
                $('#'+pvsId+'_status').val(pvsStatus);
                if (pvsStatus=="1"){
                    $(/*'#thModelDataGrid'*/pvGridId).dataGrid(
                        'setRowData',pvsId,
                        null,{display:'none'});
                }
                else {
                    $(_this).children("i").attr("class",pvsStatus==Global.STATUS_NORMAL?"glyphicon glyphicon-ban-circle":"glyphicon glyphicon-ok-circle");
                }
                $("#{0}_isModified".format(pvsId)).val("1");
            });
        return false;
    };
    /**
     * 设置grid行转大写事件
     */
    owner.setUpperCaseGridRow=function(rowId){
        for (var i=0;i<mFieldsToUpper.length;i++){
            var lvObj=$("#{0}_{1}".format(rowId, mFieldsToUpper[i]));
            lvObj.blur(function () {
                $(this).val($(this).val().toUpperCase().trim());
            });
        }
    }
    owner.setHookChangeEvent=function(rowId,rowData){
        //$("#{0}_id".format("1146065189528657920")).parent().parent().children("td").children("select.editable,input.editable");
        if ((typeof rowData.actions!="undefined") && rowData.actions=="new"){
            $("#{0}_isModified".format(rowId)).attr("isModified","1").
            parent().parent().addClass("isModified").removeClass("ui-state-highlight");
        }
        $("#{0}_id".format(rowId)).parent().parent().children("td")
            .children("select.editable,input.editable").change(function(){
              $(this).attr("isModified","1").
              parent().parent().addClass("isModified").removeClass("ui-state-highlight");
              console.log($(this).attr("id")+" is changed, value:"+$(this).val());
            }
        );
    }
    owner.beforeSubmit=function(pvForm){
        $("#inputForm input,select").filter('[isModified="1"]').each(function(i,obj){
            if (!obj.id.includes("_")){ //master table field
                $("#{0} #isModified".format($(pvForm).attr("id"))).val("1");
            }
            else{
                var lvsId=obj.id.split("_")[0];
                if ($("#{0}_isModified".format(lvsId)).length==0){
                    js.alert('忘记在inputForm里面增加<#form:hidden path="isModified"/>');
                }
                $("#{0}_isModified".format(lvsId)).val("1");
            }
        });
        return true;
    }
}(window.app={}));

$(document).ready(function(){
    if (document.getElementById("loginForm")!=null){
        $("#username").select().focus();
    }
    //设置isModified='0'
    $("form input[name='isModified']").val("0");
    $("form input,select").change(function(){
        if ($(this).attr("id")=="uploadImage")return;
        $(this).attr("isModified","1");
        console.log($(this).attr("id")+" is changed, value:"+$(this).val());
    });
    $("#inputForm input[type='radio'],input[type='checkbox']").on('ifChecked', function(event){
        if ($(this).attr("id")=="uploadImage")return;
        $(this).attr("isModified","1");
        console.log($(this).attr("id")+" is changed, value:"+$(this).val());
    });

    for (var i=0;i<mFieldsToUpper.length;i++){
        $("input[name='{0}']".format(mFieldsToUpper[i])).blur(function () {
           $(this).val($(this).val().toUpperCase().trim());
        });
    }

});