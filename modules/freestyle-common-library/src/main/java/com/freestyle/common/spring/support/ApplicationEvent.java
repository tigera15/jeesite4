package com.freestyle.common.spring.support;

import com.jeesite.common.config.Global;
import com.jeesite.modules.patch.PatchAspect;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class ApplicationEvent implements ApplicationListener<ContextRefreshedEvent> {


  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {

    PatchAspect.patchKey();
    //在容器加载完毕后获取dao层来操作数据库
    /*DataSource dataSource = (DataSource)event.getApplicationContext().getBean(DataSource.class);
    DruidDataSource dataSource1=(DruidDataSource) ((RoutingDataSource)dataSource).getTargetDataSource("default");
    dataSource1.setDefaultAutoCommit(false);*/
  }
}
