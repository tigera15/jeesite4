package com.freestyle.common.db; /****
 * com.freestyle.common.db.BeanUtil
 * Create by Rock.
 */

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class BeanUtil {

	@SuppressWarnings("unchecked")
	public static <T> Class<T> wrap(Class<T> c) {
		return c.isPrimitive() ? (Class<T>) PRIMITIVES_TO_WRAPPERS.get(c) : c;
	}

	public static Field[] getFields(Class<?> cl) {
		return cl.getFields();
	}

	/***
	 * 从 a.b.c.d的类名中提取d
	 * 
	 * @param cl
	 * @return
	 */
	public static String getClassShortName(Class<?> cl) {
		String[] items = cl.getName().split("\\.");
		return items[items.length - 1];
	}

	public static void copyFieldsValues(Object src, Object dest) {
		try {
			HashMap<String, Object> w_values = getFieldsValues(src);
			for (Entry<String, Object> entry : w_values.entrySet()) {
				setFieldValue(dest, entry.getKey(), entry.getValue());
			}
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void setFieldValue(Object bean, String fieldName, Object val)
			throws IllegalArgumentException, IllegalAccessException {
		Field[] w_fields = getFields(bean.getClass());
		for (Field f : w_fields) {
			if (f.getName().equals(fieldName)) {
				f.set(bean, val);
				break;
			}
		}
	}

	protected static final Map<Class<?>, Class<?>> PRIMITIVES_TO_WRAPPERS = new HashMap<Class<?>, Class<?>>();
	static {
		PRIMITIVES_TO_WRAPPERS.put(boolean.class, Boolean.class);
		PRIMITIVES_TO_WRAPPERS.put(byte.class, Byte.class);
		PRIMITIVES_TO_WRAPPERS.put(char.class, Character.class);
		PRIMITIVES_TO_WRAPPERS.put(double.class, Double.class);
		PRIMITIVES_TO_WRAPPERS.put(float.class, Float.class);
		PRIMITIVES_TO_WRAPPERS.put(int.class, Integer.class);
		PRIMITIVES_TO_WRAPPERS.put(long.class, Long.class);
		PRIMITIVES_TO_WRAPPERS.put(short.class, Short.class);
		PRIMITIVES_TO_WRAPPERS.put(void.class, Void.class);
	}

	public static <T> T getFieldValue(Object pvBean, String pvFieldName, Class<T> pvReturnClass) {
		Class<?> lvClazz = pvBean.getClass();
		for (; lvClazz != Object.class; lvClazz = lvClazz.getSuperclass()) {
			Field[] lvFields = lvClazz.getDeclaredFields();
			for (Field f : lvFields) {
				//System.out.println(f.getName());
				if (f.getName().equals(pvFieldName)) {
					f.setAccessible(true);
					try {
						return (T) f.get(pvBean);
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		throw new RuntimeException("Invalid field '" + pvFieldName + "'");
	}

	public static HashMap<String, Object> getFieldsValues(Object bean)
			throws IllegalAccessException, IllegalArgumentException {
		HashMap<String, Object> w_ret = new HashMap<String, Object>();
		for (Field f : getFields(bean.getClass())) {
			w_ret.put(f.getName(), f.get(bean));
		}
		return w_ret;
	}

	/***
	 * 判断此Class是否为date
	 * 
	 * @param cl
	 * @return
	 */
	public static boolean classTypeIsDate(Class<?> cl) {
		return cl.getSimpleName().toLowerCase().equals("date");
	}

}
