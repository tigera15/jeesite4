package com.freestyle.common.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

//import org.apache.commons.logging.LogFactory;

public class JsonUtils {
	// private static final Log log = LogFactory.getLog(JsonUtils.class);
	private final static ObjectMapper mapper = new ObjectMapper();

	public static String jsonFromObject(Object object) {
		/*mapper.constructType(object.getClass());
		StringWriter writer = new StringWriter();
		try {
			mapper.writeValue(writer, object);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			// log.error("Unable to serialize to json: " + object, e);
			return null;
		}
		return writer.toString();*/
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static String jsonFromObject(Object object, String encoding) {
		mapper.constructType(object.getClass());
		try {
			byte[] data = mapper.writeValueAsBytes(object);
			return new String(data, encoding);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public static String jsonPrettyFromObject(Object object) throws JsonProcessingException {
		return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
	}
	/****
	 * 从content json字串根据 TypeReference还原对象
	 * 
	 * @param content
	 * @param valueType,TypeReference
	 *            = new TypeReference<Class>() {}
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T readValue(String content, TypeReference valueType) {
		try {
			/*
			 * JsonParser jsonParser=(new
			 * JsonFactory()).createJsonParser(content); while
			 * (jsonParser.hasCurrentToken()){ jsonParser.read
			 * jsonParser.nextToken(); }
			 */
			//mapper.constructType(valueType.getClass());
			return (T) mapper.readValue(content, valueType);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/****
	 * 从content json字串根据 TypeReference还原对象
	 * 
	 * @param content
	 *            = new TypeReference<Class>() {}
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T readValue(String content,Class<T> type) {
		try {

			return (T) mapper.readValue(content, type);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public static <T> T readValue(String content, TypeReference valueType, boolean pvSkipEmptyProperties) {
		try {
			/*
			 * JsonParser jsonParser=(new
			 * JsonFactory()).createJsonParser(content); while
			 * (jsonParser.hasCurrentToken()){ jsonParser.read
			 * jsonParser.nextToken(); }
			 */
			if (pvSkipEmptyProperties) {
				// mapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				// mapper.disable(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS);
			} else {
				// mapper.enable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
			}
			mapper.constructType(valueType.getClass());
			return (T) mapper.readValue(content, valueType);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}