package com.freestyle.common.protocols;

import com.freestyle.common.utils.JsonUtils;
import com.freestyle.common.utils.Util;

import java.io.Serializable;

public class JSONData implements Serializable{
	private static final long serialVersionUID = -7719174847256301044L;
	public final static int ERROR_CODE_SUCCESS=0;		
	public int errCode;
	public String result;
	public String message;
	//public String errMsg="";
	public String errRef=""; //错误参考,一般存储数据字段名
	//public Object  result;
	public JSONData(){
		errCode=ERROR_CODE_SUCCESS;
		result="true";
		//errMsg="";
		message="";
	}
	public static String getSuccessJSonText() {
		return JsonUtils.jsonFromObject(new JSONData());
	}
	public JSONData(Exception pvException, Exception pvException2){
		this(pvException==null?pvException2:pvException);
	}
	public JSONData( Exception pvException){
		errCode=-1;
		result="false";
		message=pvException.getMessage();
		if (pvException instanceof ModifyDataException){
			result= "false";
			errRef= Util.toString(((ModifyDataException)pvException).getRefDataFields(),",");
		}
	}
	public JSONData(int pvErrCode,String pvExceptionMsg){
		errCode=pvErrCode;
		result="false";
		message=pvExceptionMsg;
	}
	public String toString(){
		return JsonUtils.jsonFromObject(this);
	}
}
