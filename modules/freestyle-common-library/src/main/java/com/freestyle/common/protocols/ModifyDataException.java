package com.freestyle.common.protocols;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.UncategorizedSQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ModifyDataException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 77104513623230540L;
	private List<String> dataFields=new ArrayList<String> ();
	public static Exception fromSqlException(Exception e) {
		if (e instanceof DataIntegrityViolationException) {
			String lvsField=e.getMessage().split("\"")[1];
			return new ModifyDataException(e.getMessage().split("\n")[1], lvsField);
		}
		else if (e instanceof UncategorizedSQLException) {
			//String lvsMsg=e.getMessage();
			return new RuntimeException(e.getMessage().split("###")[1]);
		}
		return null;
	}
	public ModifyDataException(String errMsg, String... refDataField){
		super(errMsg);
		if (refDataField!=null){
			dataFields.addAll(Arrays.asList(refDataField));
		}
	}
	public ModifyDataException(Exception e){
		super(e.getMessage());
		if (e instanceof ModifyDataException) {
			dataFields=((ModifyDataException) e).dataFields;
		}
	}
	public List<String> getRefDataFields(){
		return dataFields;
	}
	
}
