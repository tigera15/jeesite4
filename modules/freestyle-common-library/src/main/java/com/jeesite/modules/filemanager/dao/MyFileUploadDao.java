package com.jeesite.modules.filemanager.dao;

import com.jeesite.common.dao.MyCrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.file.entity.FileUpload;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@MyBatisDao
public interface MyFileUploadDao extends MyCrudDao<FileUpload> {

   @Select({"<script>delete from js_sys_file_entity where file_id=:fileid</script>"})
   void deleteByFileId(@Param("fileid") String pvsFileId);
}
