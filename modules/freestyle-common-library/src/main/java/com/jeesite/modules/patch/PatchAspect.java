package com.jeesite.modules.patch;

import com.freestyle.common.utils.PrivateUtil;
import com.jeesite.common.service.f;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;

@Aspect
@Component
public class PatchAspect {
  public static void patchKey(){
    f.i();
    String lvs="fnFm";// FilemanagerFolderService.h("\u0012\b2\u000b");
    Field lvField= null;
    try {
      Class clz=f.class;
      lvField = f.class.getDeclaredField("l");
      lvField.setAccessible(true);
      f.li lvKeys= (f.li) lvField.get(null);
      Method hash=PrivateUtil.getMethod(HashMap.class,"hash",new Class[]{Object.class});
      hash.setAccessible(true);
      int lvnHashCode= (int) hash.invoke(null,lvs);
      //lvKeys.put(lvs,"true");
      Method lvParentMethod=PrivateUtil.getMethod(HashMap.class,"putVal",new Class[]{int.class,Object.class,
              Object.class,boolean.class,boolean.class});
      lvParentMethod.setAccessible(true);
      //PrivateUtil.invoke(lvKeys,lvMethod,new Object[]{lvs,"true"});
      PrivateUtil.invoke(lvKeys,lvParentMethod,new Object[]{lvnHashCode,lvs,"true",false,true});
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  @Around("@annotation(org.apache.shiro.authz.annotation.RequiresPermissions)")
  public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
    Signature signature = pjp.getSignature();
    Object lvTarget=pjp.getTarget();
   /* if (!(lvTarget instanceof FilemanagerController)){
      return pjp.proceed();
    }
*/
    return pjp.proceed();
    /*
    LoggerFactory.getLogger("PatchAspect").info(lvTarget.toString());
    String methodName = signature.getName();
    Object[] args = pjp.getArgs();
    if (methodName.equals("save")) {
      //UUID uuid = UUID.randomUUID();
      //String info = JSON.toJSONString(args);
      //方法执行前处理
      //Object obj = pjp.proceed();
      Object obj = (new FilemanagerControllerPatch((FilemanagerController) lvTarget, (Filemanager) args[0])).save();
      //String resultStr = JSON.toJSONString(obj);
      //方法执行后处理
      return obj;
    }
    else if (methodName.equals("delete")){
      Object obj = (new FilemanagerControllerPatch((FilemanagerController) lvTarget, (Filemanager) args[0])).delete();
      return obj;
    }
    else{
      return pjp.proceed();
    }*/
  }

}
