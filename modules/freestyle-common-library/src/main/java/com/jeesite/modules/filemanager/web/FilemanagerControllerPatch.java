package com.jeesite.modules.filemanager.web;

import com.jeesite.modules.file.service.FileUploadService;
import com.jeesite.modules.file.utils.FileUploadUtils;
import com.jeesite.modules.filemanager.entity.Filemanager;
import com.jeesite.modules.filemanager.service.FilemanagerFolderService;
import com.jeesite.modules.filemanager.service.FilemanagerService;
import com.jeesite.modules.sys.utils.EmpUtils;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import static com.jeesite.common.web.BaseController.text;

public class FilemanagerControllerPatch {
  private FilemanagerController filemanagerController;
  private FilemanagerService filemanagerService;
  private Filemanager filemanager;
  private Method renderResult;
  private FileUploadService updateService;
  private FilemanagerFolderService filemanagerFolderService;
  public FilemanagerControllerPatch(FilemanagerController pvController, Filemanager pvFilemanager){
    filemanagerController=pvController;
    try {
      Field lvField = pvController.getClass().getDeclaredField("filemanagerService");
      lvField.setAccessible(true);
      filemanagerService = (FilemanagerService) lvField.get(pvController);
      lvField=filemanagerService.getClass().getDeclaredField("fileUploadService");
      lvField.setAccessible(true);
      updateService= (FileUploadService) lvField.get(filemanagerService);
      lvField=filemanagerService.getClass().getDeclaredField("filemanagerFolderService");
      lvField.setAccessible(true);
      filemanagerFolderService=(FilemanagerFolderService)lvField.get(filemanagerService);
      renderResult=pvController.getClass().getSuperclass().getDeclaredMethod("renderResult",new Class[]{ String.class,String.class});
      renderResult.setAccessible(true);
      filemanager=pvFilemanager;
    }
    catch (Exception e){
      LoggerFactory.getLogger(getClass().getSimpleName()).error("create",e);
    }
  }
  public String save(){
    //this.filemanagerService.save(filemanager);
    filemanager.setCreateBy(filemanager.getCurrentUser().getUserCode());
    filemanager.setOfficeCode(EmpUtils.getOffice().getOfficeCode());
    FileUploadUtils.saveFileUpload(filemanager.getFolderId(), filemanager.getBizType());
    String lvsMsg=null;
    try{
      lvsMsg= (String) renderResult.invoke(filemanagerController,"true", text( "上传文件成功！", null
      ));
    }
    catch (Exception e){
       throw new RuntimeException(e);
    }
    return lvsMsg;
  }

  public String delete(){
    //String lvs=FilemanagerFolderService.h("\u0012\b2\u000b");
    //f.li lvKeys=f.i();
    //lvKeys.put("fnFm","true");
    //LoggerFactory.getLogger("aa").info(lvKeys.get(lvs));
    //LoggerFactory.getLogger("aa").info(lvs);
    /*if (filemanager.getIds() != null) {
      String[] var2;
      int var3 = (var2 = filemanager.getIds()).length;

      int var4;
      for(int var10000 = var4 = 0; var10000 < var3; var10000 = var4) {
        String a;
        if (StringUtils.isNotBlank(a = var2[var4])) {
           a = StringUtils.substringBefore(a, FilemanagerFolderService.h("9"));
           a = StringUtils.substringAfter(a, FilemanagerFolderService.h("9"));
           String lvsT1=FilemanagerFolderService.h("\u0012\t\u0018\u0002\u0011\u0014");
          if (lvsT1.equals(a)) {
            FilemanagerFolder filemanagerFolder = new FilemanagerFolder(a);
            filemanagerFolderService.delete(filemanagerFolder);
          } else {
            FileUpload fileUpload = new FileUpload(a);
            updateService.delete(fileUpload);
          }
        }
        ++var4;
      }
    }*/
    filemanagerService.delete(filemanager);

    String var10001 = "true";//FilemanagerFolderService.h("\u0000\u0014\u0001\u0003");
    String var10002 = "删除文件成功！";
    String[] var10003 = new String[0];
    boolean var10005 = true;
    String lvsMsg=null;
    try{
      lvsMsg= (String) renderResult.invoke(filemanagerController,var10001, text(var10002, var10003));
    }
    catch (Exception e){
      throw new RuntimeException(e);
    }
    return lvsMsg;
  }
}
