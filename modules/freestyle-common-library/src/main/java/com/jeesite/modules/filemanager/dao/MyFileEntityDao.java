package com.jeesite.modules.filemanager.dao;

import com.jeesite.common.dao.MyCrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.file.entity.FileEntity;

@MyBatisDao
public interface MyFileEntityDao extends MyCrudDao<FileEntity> {
}
