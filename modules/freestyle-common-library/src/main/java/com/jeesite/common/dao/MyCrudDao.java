package com.jeesite.common.dao;


import com.jeesite.common.mybatis.mapper.provider.MySelectSqlProvider;
import com.jeesite.common.mybatis.mapper.provider.MyUpdateSqlProvider;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

/***
 * 增强型的CrudDao,可通过配置参数控制是否真正删除记录
 * by rocklee, 9/Jul/2019
 * @param <T>
 */
public interface MyCrudDao<T> extends CrudDao<T> {
  @UpdateProvider(
          type = MyUpdateSqlProvider.class,
          method = "delete"
  )
  @Override
  long delete(T var1) ;

  @SelectProvider(
          type = MySelectSqlProvider.class,
          method = "getByEntity"
  )
  @Override
  T getByEntity(T var1);
}
