package com.jeesite.common.services;

import com.jeesite.modules.file.entity.FileEntity;
import com.jeesite.modules.file.entity.FileUpload;
import com.jeesite.modules.filemanager.dao.MyFileEntityDao;
import com.jeesite.modules.filemanager.dao.MyFileUploadDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;

/*****
 * 附件上传的一些实用功能.
 * by rock 9/Jul/2019
 */
@Service
public class FileUploadUtil {
  @Resource
  protected MyFileUploadDao mFileUploadDao;
  @Resource
  protected MyFileEntityDao mFileEntityDao;

  public void physicalDeleteFileEntity(FileUpload pvFile){
    String lvFilePath = pvFile.getFileEntity().getFileRealPath();
    new File(lvFilePath).delete();
    mFileUploadDao.delete(pvFile);
    FileEntity lvWhere=new FileEntity();
    lvWhere.setFileId(pvFile.getFileEntity().getFileId());
    FileEntity lvFound= mFileEntityDao.getByEntity(lvWhere);
    if (lvFound!=null){
      /*lvFilePath = lvFound.getFileRealPath();
      new File(lvFilePath).delete();*/
      mFileEntityDao.phyDeleteByEntity(lvFound);
    }

  }
  /***
   * 清除一些临时文件(上传后未保存又作了删除标志的)
   */
  public void cleanUp(){
    FileUpload lvWhere=new FileUpload();
    lvWhere.setStatus(FileUpload.STATUS_DELETE);
    for (FileUpload rec:mFileUploadDao.findList(lvWhere)){
      physicalDeleteFileEntity(rec);
    }
  }
}
