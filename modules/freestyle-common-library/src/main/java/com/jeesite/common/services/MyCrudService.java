package com.jeesite.common.services;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.entity.Page;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.common.service.CrudService;
import com.jeesite.common.service.ServiceException;

public class MyCrudService<D extends CrudDao<T>, T extends DataEntity<?>, E extends DataEntity<?>> extends CrudService<D , T> {
  /*@Override
  public List<T> findList(T entity) {
    if (entity.getStatus() == null) {
      entity.setStatus("0");
    }
    List<T> lvRet=this.dao.findList(entity);
    return lvRet;
  }*/

  public   Page<E> findChildPage(E entity, CrudDao pvDao){
    Page lvPage=entity.getPage();
    if (lvPage==null){
      throw new ServiceException("entity的page属性没设置,不能调用findPage方法");
    }
    entity.getSqlMap().getWhere().and("status", QueryType.NE,"1");
    lvPage.setList(pvDao.findList(entity));
    return lvPage;
  }

  /*
  public Page<T> findPage(Page<T> page, T entity) {
      return super.findPage(page,entity);
  }*/


}
