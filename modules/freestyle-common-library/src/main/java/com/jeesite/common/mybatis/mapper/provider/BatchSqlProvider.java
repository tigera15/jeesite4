package com.jeesite.common.mybatis.mapper.provider;

import com.jeesite.common.entity.BaseEntity;

import java.util.List;

/** 批处理SQL处理器
 * Created by rocklee on 2019/8/10 23:11
 */
public class BatchSqlProvider {
   public String batchInsert(List<BaseEntity<?>> pvToInsertList){
     if (pvToInsertList.size()==0) return "";
     BaseEntity<?> lvPattern=pvToInsertList.get(0);
     StringBuilder sb=new StringBuilder();
     sb.append("INSERT INTO "+lvPattern.getSqlMap().getTable().toSql());
     sb.append("\n(");
     sb.append(lvPattern.getSqlMap().getColumn().toString());
     sb.append(")\n");
     for (BaseEntity<?> rec:pvToInsertList){

     }
     return null ;//有空再写
   }
}
