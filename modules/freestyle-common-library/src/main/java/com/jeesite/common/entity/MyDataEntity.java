package com.jeesite.common.entity;


import com.jeesite.common.config.Global;
import com.jeesite.common.validator.OptimisticLock;

/***
 *
 * @param <T>
 */
@OptimisticLock
public class MyDataEntity<T extends MyDataEntity<?>> extends DataEntity<T>
{
  public static final String DATA_MODIFIED="1";
  public static final String DATA_UNMODIFIED="0";
  public String getIsModified()
  {
    return isModified;
  }

  public void setIsModified(String isModified) {
    this.isModified = isModified;
  }

  /***
   * 增量更改的开关
   * @return
   */
  public static boolean isDeltaUpdate(){
    return Boolean.parseBoolean(Global.getProperty("freestyle.deltaUpdate","false"));
  }
  public String isModified="0"; //0 - 未修改, 1 - 修改
  public MyDataEntity(String id) {
    super(id);
  }
  public MyDataEntity() {
  }
}
