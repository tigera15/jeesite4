package com.jeesite.common.mybatis.mapper.provider;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.BaseEntity;
import com.jeesite.common.mybatis.mapper.MapperHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyUpdateSqlProvider  extends UpdateSqlProvider {
  private Logger logger = LoggerFactory.getLogger(this.getClass());
  @Override
  public String delete(BaseEntity<?> entity) {
    //long lvTm = System.currentTimeMillis();
    boolean lvRealDelete=Boolean.parseBoolean(Global.getProperty("freestyle.realDelete","false"));
    if (!lvRealDelete){
      return super.delete(entity);
    }
    return "delete from "+ MapperHelper.getTableName(MapperHelper.getTable(entity), entity)+" where id=#{id}";
  }
}
