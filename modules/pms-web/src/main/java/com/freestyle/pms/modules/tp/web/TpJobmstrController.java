/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.tp.web;

import com.freestyle.pms.modules.tp.entity.TpJobmstr;
import com.freestyle.pms.modules.tp.service.TpJobmstrService;
import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * tp_jobmstrController
 * @author rocklee
 * @version 2019-07-09
 */
@Controller
@RequestMapping(value = "${adminPath}/tp/tpJobmstr")
public class TpJobmstrController extends BaseController {

	@Autowired
	private TpJobmstrService tpJobmstrService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public TpJobmstr get(String id, boolean isNewRecord) {
		TpJobmstr lvRet= tpJobmstrService.get(id, isNewRecord);
		if (id ==null){
			lvRet.setFpChkProdSeq("Y");
			lvRet.setFpTarqty(10);
		}
		return lvRet;
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("tp:tpJobmstr:view")
	@RequestMapping(value = {"list", ""})
	public String list(TpJobmstr tpJobmstr, Model model) {
		model.addAttribute("tpJobmstr", tpJobmstr);
		return "modules/tp/tpJobmstrList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("tp:tpJobmstr:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<TpJobmstr> listData(TpJobmstr tpJobmstr, HttpServletRequest request, HttpServletResponse response) {
		tpJobmstr.setPage(new Page<>(request, response));
		Page<TpJobmstr> page = tpJobmstrService.findPage(tpJobmstr);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("tp:tpJobmstr:view")
	@RequestMapping(value = "form")
	public String form(TpJobmstr tpJobmstr, Model model) {
		model.addAttribute("tpJobmstr", tpJobmstr);
		return "modules/tp/tpJobmstrForm";
	}

	/**
	 * 保存工单主档
	 */
	@RequiresPermissions("tp:tpJobmstr:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated TpJobmstr tpJobmstr) {
		tpJobmstrService.save(tpJobmstr);
		return renderResult(Global.TRUE, text("保存工单主档成功！"));
	}
	
	/**
	 * 停用工单主档
	 */
	@RequiresPermissions("tp:tpJobmstr:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(TpJobmstr tpJobmstr) {
		tpJobmstr.setStatus(TpJobmstr.STATUS_DISABLE);
		tpJobmstrService.updateStatus(tpJobmstr);
		return renderResult(Global.TRUE, text("停用工单主档成功"));
	}
	
	/**
	 * 启用工单主档
	 */
	@RequiresPermissions("tp:tpJobmstr:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(TpJobmstr tpJobmstr) {
		tpJobmstr.setStatus(TpJobmstr.STATUS_NORMAL);
		tpJobmstrService.updateStatus(tpJobmstr);
		return renderResult(Global.TRUE, text("启用工单主档成功"));
	}
	
	/**
	 * 删除工单主档
	 */
	@RequiresPermissions("tp:tpJobmstr:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(TpJobmstr tpJobmstr) {
		tpJobmstrService.delete(tpJobmstr);
		return renderResult(Global.TRUE, text("删除工单主档成功！"));
	}
	
}