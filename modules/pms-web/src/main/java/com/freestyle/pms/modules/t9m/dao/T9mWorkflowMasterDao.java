/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.t9m.dao;

import com.freestyle.pms.modules.t9m.entity.T9mWorkflowMaster;
import com.jeesite.common.dao.MyCrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;

/**
 * t9m_workflow_masterDAO接口
 * @author rocklee
 * @version 2019-07-20
 */
@MyBatisDao
public interface T9mWorkflowMasterDao extends MyCrudDao<T9mWorkflowMaster> {
	
}