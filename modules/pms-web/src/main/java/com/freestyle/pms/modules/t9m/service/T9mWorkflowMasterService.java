/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.t9m.service;

import com.freestyle.pms.modules.t9m.dao.T9mJobGroupDao;
import com.freestyle.pms.modules.t9m.dao.T9mWorkflowMasterDao;
import com.freestyle.pms.modules.t9m.entity.T9mJobGroup;
import com.freestyle.pms.modules.t9m.entity.T9mWorkflowMaster;
import com.jeesite.common.entity.MyDataEntity;
import com.jeesite.common.entity.Page;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.common.services.FileUploadUtil;
import com.jeesite.common.services.MyCrudService;
import com.jeesite.modules.file.entity.FileUpload;
import com.jeesite.modules.file.utils.FileUploadUtils;
import com.jeesite.modules.filemanager.dao.MyFileUploadDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * t9m_workflow_masterService
 * @author rocklee
 * @version 2019-07-20
 */
@Service
@Transactional(readOnly=true)
public class T9mWorkflowMasterService extends MyCrudService<T9mWorkflowMasterDao, T9mWorkflowMaster,T9mJobGroup> {
	final  static  String file_type="t9mWorkflowMaster_image";
	@Resource
	protected MyFileUploadDao mFileUploadDao;
	@Resource
	protected FileUploadUtil mFileUploadUtil;
	@Autowired
	private T9mJobGroupDao t9mJobGroupDao;
	
	/**
	 * 获取单条数据
	 * @param t9mWorkflowMaster
	 * @return
	 */
	@Override
	public T9mWorkflowMaster get(T9mWorkflowMaster t9mWorkflowMaster) {
		T9mWorkflowMaster entity = super.get(t9mWorkflowMaster);
		if (entity != null){
			T9mJobGroup t9mJobGroup = new T9mJobGroup(entity);
			t9mJobGroup.setStatus(null);
			//t9mJobGroup.setStatus(T9mJobGroup.STATUS_NORMAL);
			t9mJobGroup.getSqlMap().getWhere().and("status", QueryType.NE,"1")
							.and("f9m_workflow_parent_id",QueryType.EQ,entity.getId());
			t9mJobGroup.getSqlMap().getOrder().setOrderBy("f9m_seq");
			entity.setT9mJobGroupList(t9mJobGroupDao.findList(t9mJobGroup));
		}
		return entity;
	}
	
	/**
	 * 查询分页数据
	 * @param t9mWorkflowMaster 查询条件
	 * @return
	 */
	@Override
	public Page<T9mWorkflowMaster> findPage(T9mWorkflowMaster t9mWorkflowMaster) {
		return super.findPage(t9mWorkflowMaster);
	}

	public Page<T9mWorkflowMaster> findPageByPrefix(T9mWorkflowMaster t9mWorkflowMaster) {
		t9mWorkflowMaster.getSqlMap().getWhere().and("f9m_workflow",QueryType.GTE,t9mWorkflowMaster.getF9mWorkflow());
		t9mWorkflowMaster.setF9mWorkflow(null);
		return super.findPage(t9mWorkflowMaster);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param t9mWorkflowMaster
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(T9mWorkflowMaster t9mWorkflowMaster) {

		boolean lvDeltaUpdate= MyDataEntity.isDeltaUpdate();
		if (t9mWorkflowMaster.isModified.equals(MyDataEntity.DATA_MODIFIED)||!lvDeltaUpdate) {
			super.save(t9mWorkflowMaster);
		}
		// 保存上传图片
		FileUploadUtils.saveFileUpload(t9mWorkflowMaster.getId(), file_type);


		FileUpload lvWhere=new FileUpload();
		lvWhere.setBizKey(t9mWorkflowMaster.getId());
		lvWhere.setStatus(FileUpload.STATUS_DELETE);
		lvWhere.getSqlMap().getWhere().disableAutoAddStatusWhere();
		for (FileUpload item:mFileUploadDao.findList(lvWhere)){
			if (item.getStatus().equals(FileUpload.STATUS_DELETE)) {
				mFileUploadUtil.physicalDeleteFileEntity(item);
			}
		}
		mFileUploadUtil.cleanUp(); //清除垃圾文件,应该放在cronjob里面跑


		// 保存 T9mWorkflowMaster子表
		for (T9mJobGroup t9mJobGroup : t9mWorkflowMaster.getT9mJobGroupList()){
			if (lvDeltaUpdate&&!t9mJobGroup.isModified.equals(MyDataEntity.DATA_MODIFIED))continue;
			if (!T9mJobGroup.STATUS_DELETE.equals(t9mJobGroup.getStatus())){
				t9mJobGroup.setF9mWorkflowParentId(t9mWorkflowMaster);
				if (t9mJobGroup.getIsNewRecord()){
					t9mJobGroup.setF9mWorkflowParentId(t9mWorkflowMaster);
					t9mJobGroupDao.insert(t9mJobGroup);
				}else{
					t9mJobGroupDao.update(t9mJobGroup);
				}
			}else{
				t9mJobGroupDao.delete(t9mJobGroup);
			}
		}
	}
	
	/**
	 * 更新状态
	 * @param t9mWorkflowMaster
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(T9mWorkflowMaster t9mWorkflowMaster) {
		super.updateStatus(t9mWorkflowMaster);
	}
	
	/**
	 * 删除数据
	 * @param t9mWorkflowMaster
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(T9mWorkflowMaster t9mWorkflowMaster) {
		super.delete(t9mWorkflowMaster);
		T9mJobGroup t9mJobGroup = new T9mJobGroup();
		t9mJobGroup.setF9mWorkflowParentId(t9mWorkflowMaster);
		t9mJobGroupDao.deleteByEntity(t9mJobGroup);
		for (FileUpload item:FileUploadUtils.findFileUpload(t9mJobGroup.getId(),file_type)){
			mFileUploadUtil.physicalDeleteFileEntity(item);
		}
	}


	
}