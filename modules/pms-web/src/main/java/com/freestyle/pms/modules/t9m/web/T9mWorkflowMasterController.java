/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.t9m.web;

import com.freestyle.pms.modules.t9m.entity.T9mWorkflowMaster;
import com.freestyle.pms.modules.t9m.service.T9mWorkflowMasterService;
import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * t9m_workflow_masterController
 * @author rocklee
 * @version 2019-07-20
 */
@Controller
@RequestMapping(value = "${adminPath}/t9m/t9mWorkflowMaster")
public class T9mWorkflowMasterController extends BaseController {

	@Autowired
	private T9mWorkflowMasterService t9mWorkflowMasterService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public T9mWorkflowMaster get(String id, boolean isNewRecord) {
		return t9mWorkflowMasterService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("t9m:t9mWorkflowMaster:view")
	@RequestMapping(value = {"list", ""})
	public String list(@RequestParam("type")String type, T9mWorkflowMaster t9mWorkflowMaster, Model model) {
		model.addAttribute("t9mWorkflowMaster", t9mWorkflowMaster);
		model.addAttribute("type",type);
		return "modules/t9m/t9mWorkflowMasterList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("t9m:t9mWorkflowMaster:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<T9mWorkflowMaster> listData(@RequestParam("type")String pvsType, T9mWorkflowMaster t9mWorkflowMaster, HttpServletRequest request, HttpServletResponse response) {
		if (pvsType.equals("J")){
			t9mWorkflowMaster.setF9mIsTemplate("N");
		}
		else{
			t9mWorkflowMaster.setF9mIsTemplate("Y");
		}
		t9mWorkflowMaster.setPage(new Page<>(request, response));
		Page<T9mWorkflowMaster> page = t9mWorkflowMasterService.findPage(t9mWorkflowMaster);
		return page;
	}
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("t9m:t9mWorkflowMaster:view")
	@RequestMapping(value = "listData-prefix")
	@ResponseBody
	public Page<T9mWorkflowMaster> listDataPrefix(T9mWorkflowMaster t9mWorkflowMaster, HttpServletRequest request, HttpServletResponse response) {
		t9mWorkflowMaster.setPage(new Page<>(request, response));
		Page<T9mWorkflowMaster> page = t9mWorkflowMasterService.findPageByPrefix(t9mWorkflowMaster);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("t9m:t9mWorkflowMaster:view")
	@RequestMapping(value = "form")
	public String form(T9mWorkflowMaster t9mWorkflowMaster, Model model) {
		model.addAttribute("t9mWorkflowMaster", t9mWorkflowMaster);
		return "modules/t9m/t9mWorkflowMasterForm";
	}

	/**
	 * 保存生产流程模板
	 */
	@RequiresPermissions("t9m:t9mWorkflowMaster:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated T9mWorkflowMaster t9mWorkflowMaster) {
		t9mWorkflowMasterService.save(t9mWorkflowMaster);
		return renderResult(Global.TRUE, text("保存生产流程模板成功！"));
	}
	
	/**
	 * 停用生产流程模板
	 */
	@RequiresPermissions("t9m:t9mWorkflowMaster:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(T9mWorkflowMaster t9mWorkflowMaster) {
		t9mWorkflowMaster.setStatus(T9mWorkflowMaster.STATUS_DISABLE);
		t9mWorkflowMasterService.updateStatus(t9mWorkflowMaster);
		return renderResult(Global.TRUE, text("停用生产流程模板成功"));
	}
	
	/**
	 * 启用生产流程模板
	 */
	@RequiresPermissions("t9m:t9mWorkflowMaster:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(T9mWorkflowMaster t9mWorkflowMaster) {
		t9mWorkflowMaster.setStatus(T9mWorkflowMaster.STATUS_NORMAL);
		t9mWorkflowMasterService.updateStatus(t9mWorkflowMaster);
		return renderResult(Global.TRUE, text("启用生产流程模板成功"));
	}
	
	/**
	 * 删除生产流程模板
	 */
	@RequiresPermissions("t9m:t9mWorkflowMaster:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(T9mWorkflowMaster t9mWorkflowMaster) {
		t9mWorkflowMasterService.delete(t9mWorkflowMaster);
		return renderResult(Global.TRUE, text("删除生产流程模板成功！"));
	}
	
}