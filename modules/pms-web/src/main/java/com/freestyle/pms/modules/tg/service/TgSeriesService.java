/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.tg.service;

import com.freestyle.pms.modules.tg.dao.TgSeriesDao;
import com.freestyle.pms.modules.tg.dao.ThModelDao;
import com.freestyle.pms.modules.tg.entity.TgSeries;
import com.freestyle.pms.modules.tg.entity.ThModel;
import com.jeesite.common.entity.MyDataEntity;
import com.jeesite.common.entity.Page;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.common.services.MyCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 产品系列Service
 * @author rocklee
 * @version 2019-07-01
 */
@Service
@Transactional(readOnly=true)
public class TgSeriesService extends MyCrudService<TgSeriesDao, TgSeries,ThModel> {
	
	@Autowired
	private ThModelDao thModelDao;
	@Autowired
	protected  TgSeriesDao tgSeriesDao;
	
	/**
	 * 获取单条数据
	 * @param tgSeries
	 * @return
	 */
	/*public TgSeries get(TgSeries tgSeries,Page<ThModel> pvPage) {
		TgSeries entity = super.get(tgSeries);
		if (entity != null){
			ThModel thModel = new ThModel(entity);
			thModel.setPage(pvPage); //by rock 5/Jul/2019
			thModel.setStatus(ThModel.STATUS_NORMAL); //Mybatis语句的where中没体现出来
			entity.setThModelList(thModelDao.findList(thModel));
		}
		return entity;
	}*/
	@Transactional(readOnly = false)
	public TgSeries get(TgSeries tgSeries,boolean isNewRecord){
		String lvsSeries=tgSeries.getFgSeries();
		tgSeries.setFgSeries(null);
		if (isNewRecord) {
			return new TgSeries();
		}
		if (lvsSeries!=null&&!lvsSeries.isEmpty()){ //按series取得记录时
		  tgSeries.getSqlMap().getWhere().and("fg_series", QueryType.EQ, lvsSeries);
		}
		return tgSeriesDao.getByEntity(tgSeries);
	}
	public ThModel getChild(ThModel thModel){
		String lvsModel=thModel.getFhModel();
		thModel.setFhModel(null);
		if (lvsModel!=null){
			thModel.getSqlMap().getWhere().and("fh_model", QueryType.EQ,lvsModel);
		}
		//从表的话, 必须手动增加status,不然会被忽略
		thModel.getSqlMap().getWhere().and("status",QueryType.EQ,ThModel.STATUS_NORMAL);
		return thModelDao.getByEntity(thModel);
	}
	
	/**
	 * 查询分页数据
	 * @param tgSeries 查询条件
	 * @return
	 */
	@Override
	public Page<TgSeries> findPage(TgSeries tgSeries) {
		if (tgSeries.getPage().getOrderBy()==null||tgSeries.getPage().getOrderBy().equals("")){
			tgSeries.getPage().setOrderBy("fg_series");
		}
		return super.findPage(tgSeries);
	}
	/**
	 * 查询分页数据
	 * @param thModel 查询条件
	 * @return
	 */
	public Page<ThModel> findChildPage(ThModel thModel) {
		Page<ThModel> lvPage=super.findChildPage(thModel,thModelDao);
		return lvPage;
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param tgSeries
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(TgSeries tgSeries) {
    boolean lvDeltaUpdate=MyDataEntity.isDeltaUpdate();
	  if (tgSeries.isModified.equals(MyDataEntity.DATA_MODIFIED)||!lvDeltaUpdate) {
      super.save(tgSeries);
    }
		// 保存 TgSeries子表
		for (ThModel thModel : tgSeries.getThModelList()){
		  if (lvDeltaUpdate&&!thModel.isModified.equals(MyDataEntity.DATA_MODIFIED))continue;
			if (!ThModel.STATUS_DELETE.equals(thModel.getStatus())){
				thModel.setFhTgId(tgSeries);
				if (thModel.getIsNewRecord()){
					thModelDao.insert(thModel);
				}else{
					thModelDao.update(thModel);
				}
			}else{
				thModelDao.delete(thModel);
			}
		}
	}
	
	/**
	 * 更新状态
	 * @param tgSeries
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(TgSeries tgSeries) {
		super.updateStatus(tgSeries);
	}

	@Transactional(readOnly=false)
	public void updateChildStatus(ThModel entity) {
		thModelDao.updateStatus(entity);
	}

	
	/**
	 * 删除数据
	 * @param tgSeries
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(TgSeries tgSeries) {
		super.delete(tgSeries);
		ThModel thModel = new ThModel();
		thModel.setFhTgId(tgSeries);
		thModelDao.delete(thModel);
	}
	
}