/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.tg.dao;

import com.freestyle.pms.modules.tg.entity.ThModel;
import com.jeesite.common.dao.MyCrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;

/**
 * 产品系列DAO接口
 * @author rocklee
 * @version 2019-07-01
 */
@MyBatisDao
public interface ThModelDao extends MyCrudDao<ThModel> {

}