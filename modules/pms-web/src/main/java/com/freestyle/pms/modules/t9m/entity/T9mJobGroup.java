/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.t9m.entity;

import com.jeesite.common.entity.MyDataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * t9m_workflow_masterEntity
 * @author rocklee
 * @version 2019-07-20
 */
@Table(name="t9m_job_group", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="f9m_group", attrName="f9mGroup", label="站位名称"),
		@Column(name="f9m_seq", attrName="f9mSeq", label="排序", isQuery=false),
		@Column(name="f9m_rpd_rtn_group", attrName="f9mRpdRtnGroup", label="维修后返回站位", isQuery=false),
		@Column(name="f9m_is_template", attrName="f9mIsTemplate", label="生产流程模板", isQuery=false),
		@Column(name="f9m_line_type", attrName="f9mLineType", label="生产线类型"),
		@Column(name="f9m_op_type", attrName="f9mOpType", label="站位类型"),
		@Column(name="f9m_stopline_control", attrName="f9mStoplineControl", label="停拉控制"),
		@Column(name="f9m_interval", attrName="f9mInterval", label="间隔分钟数", isQuery=false),
		@Column(name="create_by", attrName="createBy", label="创建人", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false),
		@Column(name="update_by", attrName="updateBy", label="修改人", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="修改时间", isQuery=false),
		@Column(name="status", attrName="status", label="状态", isQuery=false),
		@Column(name="f9m_workflow_parent_id", attrName="f9mWorkflowParentId.id", label="f9m_workflow_parent_id", isQuery=false),
	}, orderBy="a.create_date ASC"
)
public class T9mJobGroup extends MyDataEntity<T9mJobGroup> {
	
	private static final long serialVersionUID = 1L;
	private String f9mGroup;		// 站位名称
	private Long f9mSeq;		// 排序
	private String f9mRpdRtnGroup;		// 维修后返回站位
	private String f9mIsTemplate;		// 生产流程模板
	private String f9mLineType;		// 生产线类型
	private String f9mOpType;		// 站位类型
	private String f9mStoplineControl;		// 停拉控制
	private Long f9mInterval;		// 间隔分钟数
	private T9mWorkflowMaster f9mWorkflowParentId;		// f9m_workflow_parent_id 父类
	
	public T9mJobGroup() {
		this(null);
	}


	public T9mJobGroup(T9mWorkflowMaster f9mWorkflowParentId){
		this.f9mWorkflowParentId = f9mWorkflowParentId;
	}
	
	@Length(min=0, max=10, message="站位名称长度不能超过 10 个字符")
	public String getF9mGroup() {
		return f9mGroup;
	}

	public void setF9mGroup(String f9mGroup) {
		this.f9mGroup = f9mGroup;
	}
	
	public Long getF9mSeq() {
		return f9mSeq;
	}

	public void setF9mSeq(Long f9mSeq) {
		this.f9mSeq = f9mSeq;
	}
	
	@NotBlank(message="维修后返回站位不能为空")
	public String getF9mRpdRtnGroup() {
		return f9mRpdRtnGroup;
	}

	public void setF9mRpdRtnGroup(String f9mRpdRtnGroup) {
		this.f9mRpdRtnGroup = f9mRpdRtnGroup;
	}
	
	@Length(min=0, max=1, message="生产流程模板长度不能超过 1 个字符")
	public String getF9mIsTemplate() {
		return f9mIsTemplate;
	}

	public void setF9mIsTemplate(String f9mIsTemplate) {
		this.f9mIsTemplate = f9mIsTemplate;
	}
	
	@NotBlank(message="生产线类型不能为空")
	@Length(min=0, max=1, message="生产线类型长度不能超过 1 个字符")
	public String getF9mLineType() {
		return f9mLineType;
	}

	public void setF9mLineType(String f9mLineType) {
		this.f9mLineType = f9mLineType;
	}
	
	@NotBlank(message="站位类型不能为空")
	@Length(min=0, max=6, message="站位类型长度不能超过 6 个字符")
	public String getF9mOpType() {
		return f9mOpType;
	}

	public void setF9mOpType(String f9mOpType) {
		this.f9mOpType = f9mOpType;
	}
	
	@NotBlank(message="停拉控制不能为空")
	@Length(min=0, max=1, message="停拉控制长度不能超过 1 个字符")
	public String getF9mStoplineControl() {
		return f9mStoplineControl;
	}

	public void setF9mStoplineControl(String f9mStoplineControl) {
		this.f9mStoplineControl = f9mStoplineControl;
	}
	
	@NotNull(message="间隔分钟数不能为空")
	public Long getF9mInterval() {
		return f9mInterval;
	}

	public void setF9mInterval(Long f9mInterval) {
		this.f9mInterval = f9mInterval;
	}
	
	@Length(min=0, max=64, message="f9m_workflow_parent_id长度不能超过 64 个字符")
	public T9mWorkflowMaster getF9mWorkflowParentId() {
		return f9mWorkflowParentId;
	}

	public void setF9mWorkflowParentId(T9mWorkflowMaster f9mWorkflowParentId) {
		this.f9mWorkflowParentId = f9mWorkflowParentId;
	}
	
}