/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.td.service;

import com.freestyle.pms.modules.td.dao.TdTest1Dao;
import com.freestyle.pms.modules.td.entity.TdTest1;
import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 单一表测试功能Service
 * @author rocklee
 * @version 2019-06-27
 */
@Service
@Transactional(readOnly=true)
public class TdTest1Service extends CrudService<TdTest1Dao, TdTest1> {

	/**
	 * 保存数据（插入或更新）
	 * @param tdTest1
	 */
	@Override
	@Transactional(rollbackFor = RuntimeException.class)
	public void save(TdTest1 tdTest1) {
		super.save(tdTest1);
	}

	/**
	 * 获取单条数据
	 * @param tdTest1
	 * @return
	 */
	@Override
	public TdTest1 get(TdTest1 tdTest1) {
		return super.get(tdTest1);
	}
	
	/**
	 * 查询分页数据
	 * @param tdTest1 查询条件
	 * @return
	 */
	@Override
	public Page<TdTest1> findPage(TdTest1 tdTest1) {
		//
		return super.findPage(tdTest1);
	}
	

	
	/**
	 * 更新状态
	 * @param tdTest1
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(TdTest1 tdTest1) {
		super.updateStatus(tdTest1);
	}
	
	/**
	 * 删除数据
	 * @param tdTest1
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(TdTest1 tdTest1) {
		super.delete(tdTest1);
	}
	
}