/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.t9m.entity;

import com.jeesite.common.collect.ListUtils;
import com.jeesite.common.entity.MyDataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * t9m_workflow_masterEntity
 * @author rocklee
 * @version 2019-07-20
 */
@Table(name="t9m_workflow_master", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="f9m_workflow", attrName="f9mWorkflow", label="生产流程名称"),
		@Column(name="f9m_remark", attrName="f9mRemark", label="备注"),
		@Column(name="f9m_is_template", attrName="f9mIsTemplate", label="流程模板"),
		@Column(name="create_by", attrName="createBy", label="创建人", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false),
		@Column(name="update_by", attrName="updateBy", label="修改人", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="修改时间", isQuery=false),
		@Column(name="status", attrName="status", label="状态", isUpdate=false),
	}, orderBy="a.update_date DESC"
)
public class T9mWorkflowMaster extends MyDataEntity<T9mWorkflowMaster> {
	
	private static final long serialVersionUID = 1L;
	private String f9mWorkflow;		// 生产流程名称
	private String f9mRemark;		// 备注
	private String f9mIsTemplate;		// 流程模板
	private List<T9mJobGroup> t9mJobGroupList = ListUtils.newArrayList();		// 子表列表
	
	public T9mWorkflowMaster() {
		this(null);
	}

	public T9mWorkflowMaster(String id){
		super(id);
	}
	
	@NotBlank(message="生产流程名称不能为空")
	@Length(min=0, max=18, message="生产流程名称长度不能超过 18 个字符")
	public String getF9mWorkflow() {
		return f9mWorkflow;
	}

	public void setF9mWorkflow(String f9mWorkflow) {
		this.f9mWorkflow = f9mWorkflow;
	}
	
	@Length(min=0, max=100, message="备注长度不能超过 100 个字符")
	public String getF9mRemark() {
		return f9mRemark;
	}

	public void setF9mRemark(String f9mRemark) {
		this.f9mRemark = f9mRemark;
	}
	
	@NotBlank(message="流程模板不能为空")
	@Length(min=0, max=1, message="流程模板长度不能超过 1 个字符")
	public String getF9mIsTemplate() {
		return f9mIsTemplate;
	}

	public void setF9mIsTemplate(String f9mIsTemplate) {
		this.f9mIsTemplate = f9mIsTemplate;
	}
	
	public List<T9mJobGroup> getT9mJobGroupList() {
		return t9mJobGroupList;
	}

	public void setT9mJobGroupList(List<T9mJobGroup> t9mJobGroupList) {
		this.t9mJobGroupList = t9mJobGroupList;
	}
	
}