/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.td.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.freestyle.pms.modules.td.entity.TdTest1;

/**
 * 单一表测试功能DAO接口
 * @author rocklee
 * @version 2019-06-27
 */
@MyBatisDao
public interface TdTest1Dao extends CrudDao<TdTest1> {
	
}