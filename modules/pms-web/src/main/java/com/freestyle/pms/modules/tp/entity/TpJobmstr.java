/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.tp.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * tp_jobmstrEntity
 * @author rocklee
 * @version 2019-07-09
 */
@Table(name="tp_jobmstr", alias="a", columns={
		@Column(name="fp_jobno", attrName="fpJobno", label="工单号码", isUpdate=true),
		@Column(name="fp_series", attrName="fpSeries", label="产品系列"),
		@Column(name="fp_model", attrName="fpModel", label="产品型号"),
		@Column(name="fp_delcnty", attrName="fpDelcnty", label="销售地区/国家"),
		@Column(name="fp_tarqty", attrName="fpTarqty", label="计划产量", isQuery=false),
		@Column(name="fp_chk_prod_seq", attrName="fpChkProdSeq", label="站位顺序检查"),
		@Column(name="fp_cust", attrName="fpCust", label="客户"),
		@Column(name="fp_brand", attrName="fpBrand", label="牌子"),
		@Column(name="fp_finish_dt", attrName="fpFinishDt", label="完成日期"),
		@Column(name="status", attrName="status", label="状态", isUpdate=false),
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="fp_plant", attrName="fpPlant", label="fp_plant", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_mfgjob", attrName="fpMfgjob", label="fp_mfgjob", isQuery=false),
		@Column(name="fp_attach", attrName="fpAttach", label="fp_attach", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_keypart", attrName="fpKeypart", label="fp_keypart", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_pcb_no", attrName="fpPcbNo", label="fp_pcb_no", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_pcb_prg", attrName="fpPcbPrg", label="fp_pcb_prg", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_pcb_side", attrName="fpPcbSide", label="fp_pcb_side", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_pcb_type", attrName="fpPcbType", label="fp_pcb_type", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_component", attrName="fpComponent", label="fp_component", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_comp_multi", attrName="fpCompMulti", label="fp_comp_multi", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_chk_type", attrName="fpChkType", label="fp_chk_type", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_main_jobno", attrName="fpMainJobno", label="fp_main_jobno", isQuery=false),
		@Column(name="fp_firm_ver", attrName="fpFirmVer", label="fp_firm_ver", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_refresh_a", attrName="fpRefreshA", label="fp_refresh_a", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_refresh_b", attrName="fpRefreshB", label="fp_refresh_b", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_model_ver", attrName="fpModelVer", label="fp_model_ver", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_mi_qty", attrName="fpMiQty", label="fp_mi_qty", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_rd2ship_qty", attrName="fpRd2shipQty", label="fp_rd2ship_qty", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_ml_qty", attrName="fpMlQty", label="fp_ml_qty", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_ml_rmk", attrName="fpMlRmk", label="fp_ml_rmk", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_rev_no", attrName="fpRevNo", label="-＞f26_rev_no", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_remark", attrName="fpRemark", label="fp_remark", isQuery=false),
		@Column(name="fp_last_modify_dt", attrName="fpLastModifyDt", label="-rework datetime", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_other_mfg", attrName="fpOtherMfg", label="Y-其他分廠生產N-本廠生產", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cust_model", attrName="fpCustModel", label="fp_cust_model", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_ship_outstd", attrName="fpShipOutstd", label="fp_ship_outstd", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_expect_efficiency", attrName="fpExpectEfficiency", label="fp_expect_efficiency", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_pp_flag", attrName="fpPpFlag", label="fp_pp_flag", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_dev_info", attrName="fpDevInfo", label="fp_dev_info", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_processor", attrName="fpCfgProcessor", label="fp_cfg_processor", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_os", attrName="fpCfgOs", label="fp_cfg_os", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_display", attrName="fpCfgDisplay", label="fp_cfg_display", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_storage", attrName="fpCfgStorage", label="fp_cfg_storage", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_ram", attrName="fpCfgRam", label="fp_cfg_ram", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_color", attrName="fpCfgColor", label="fp_cfg_color", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_part", attrName="fpCfgPart", label="fp_cfg_part", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_ean", attrName="fpCfgEan", label="fp_cfg_ean", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_cust_model", attrName="fpCfgCustModel", label="fp_cfg_cust_model", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_desc", attrName="fpCfgDesc", label="fp_cfg_desc", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_wlan", attrName="fpCfgWlan", label="fp_cfg_wlan", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_camera", attrName="fpCfgCamera", label="fp_cfg_camera", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_case_pack", attrName="fpCfgCasePack", label="fp_cfg_case_pack", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_gw", attrName="fpCfgGw", label="fp_cfg_gw", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_nw", attrName="fpCfgNw", label="fp_cfg_nw", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_ship_prod", attrName="fpCfgShipProd", label="fp_cfg_ship_prod", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_storage2", attrName="fpCfgStorage2", label="fp_cfg_storage2", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_warranty", attrName="fpCfgWarranty", label="fp_cfg_warranty", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_kb", attrName="fpCfgKb", label="fp_cfg_kb", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_fingerprint", attrName="fpCfgFingerprint", label="fp_cfg_fingerprint", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_marketingname", attrName="fpCfgMarketingname", label="fp_cfg_marketingname", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_cust_model_ship", attrName="fpCfgCustModelShip", label="fp_cfg_cust_model_ship", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_workflow_template", attrName="fpWorkflowTemplate", label="fp_workflow_template", isInsert=true, isUpdate=true, isQuery=false),
		@Column(name="fp_cfg_upc", attrName="fpCfgUpc", label="fp_cfg_upc", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_lte", attrName="fpCfgLte", label="fp_cfg_lte", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_resolution", attrName="fpResolution", label="fp_resolution", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_wwan", attrName="fpCfgWwan", label="fp_cfg_wwan", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_warrantcty", attrName="fpCfgWarrantcty", label="fp_cfg_warrantcty", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_prod_bom", attrName="fpProdBom", label="fp_prod_bom", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_winkeyitemid", attrName="fpCfgWinkeyitemid", label="fp_cfg_winkeyitemid", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_customer_po", attrName="fpCustomerPo", label="fp_customer_po", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fp_cfg_accessory", attrName="fpCfgAccessory", label="fp_cfg_accessory", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="create_by", attrName="createBy", label="create_by", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="create_date", isUpdate=false, isQuery=false),
		@Column(name="update_by", attrName="updateBy", label="最后修改人", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="最后修改日期", isQuery=false),
	}, orderBy="a.update_date DESC"
)
public class TpJobmstr extends DataEntity<TpJobmstr> {
	
	private static final long serialVersionUID = 1L;
	private String fpJobno;		// 工单号码
	private String fpSeries;		// 产品系列
	private String fpModel;		// 产品型号
	private String fpDelcnty;		// 销售地区/国家
	private Integer fpTarqty;		// 计划产量
	private String fpChkProdSeq;		// 站位顺序检查
	private String fpCust;		// 客户
	private String fpBrand;		// 牌子
	private Date fpFinishDt;		// 完成日期
	private String fpPlant;		// fp_plant
	private String fpMfgjob;		// fp_mfgjob
	private String fpAttach;		// fp_attach
	private String fpKeypart;		// fp_keypart
	private String fpPcbNo;		// fp_pcb_no
	private String fpPcbPrg;		// fp_pcb_prg
	private String fpPcbSide;		// fp_pcb_side
	private String fpPcbType;		// fp_pcb_type
	private Integer fpComponent;		// fp_component
	private Integer fpCompMulti;		// fp_comp_multi
	private String fpChkType;		// fp_chk_type
	private String fpMainJobno;		// fp_main_jobno
	private String fpFirmVer;		// fp_firm_ver
	private Long fpRefreshA;		// fp_refresh_a
	private Long fpRefreshB;		// fp_refresh_b
	private String fpModelVer;		// fp_model_ver
	private Integer fpMiQty;		// fp_mi_qty
	private Integer fpRd2shipQty;		// fp_rd2ship_qty
	private Integer fpMlQty;		// fp_ml_qty
	private String fpMlRmk;		// fp_ml_rmk
	private String fpRevNo;		// -＞f26_rev_no
	private String fpRemark;		// fp_remark
	private Date fpLastModifyDt;		// -rework datetime
	private String fpOtherMfg;		// Y-其他分廠生產N-本廠生產
	private String fpCustModel;		// fp_cust_model
	private String fpShipOutstd;		// fp_ship_outstd
	private Double fpExpectEfficiency;		// fp_expect_efficiency
	private String fpPpFlag;		// fp_pp_flag
	private String fpDevInfo;		// fp_dev_info
	private String fpCfgProcessor;		// fp_cfg_processor
	private String fpCfgOs;		// fp_cfg_os
	private String fpCfgDisplay;		// fp_cfg_display
	private String fpCfgStorage;		// fp_cfg_storage
	private String fpCfgRam;		// fp_cfg_ram
	private String fpCfgColor;		// fp_cfg_color
	private String fpCfgPart;		// fp_cfg_part
	private String fpCfgEan;		// fp_cfg_ean
	private String fpCfgCustModel;		// fp_cfg_cust_model
	private String fpCfgDesc;		// fp_cfg_desc
	private String fpCfgWlan;		// fp_cfg_wlan
	private String fpCfgCamera;		// fp_cfg_camera
	private String fpCfgCasePack;		// fp_cfg_case_pack
	private String fpCfgGw;		// fp_cfg_gw
	private String fpCfgNw;		// fp_cfg_nw
	private String fpCfgShipProd;		// fp_cfg_ship_prod
	private String fpCfgStorage2;		// fp_cfg_storage2
	private String fpCfgWarranty;		// fp_cfg_warranty
	private String fpCfgKb;		// fp_cfg_kb
	private String fpCfgFingerprint;		// fp_cfg_fingerprint
	private String fpCfgMarketingname;		// fp_cfg_marketingname
	private String fpCfgCustModelShip;		// fp_cfg_cust_model_ship
	private String fpWorkflowTemplate;		// fp_workflow_template
	private String fpCfgUpc;		// fp_cfg_upc
	private String fpCfgLte;		// fp_cfg_lte
	private String fpResolution;		// fp_resolution
	private String fpCfgWwan;		// fp_cfg_wwan
	private String fpCfgWarrantcty;		// fp_cfg_warrantcty
	private String fpProdBom;		// fp_prod_bom
	private String fpCfgWinkeyitemid;		// fp_cfg_winkeyitemid
	private String fpCustomerPo;		// fp_customer_po
	private String fpCfgAccessory;		// fp_cfg_accessory
	
	public TpJobmstr() {
		this(null);
	}

	public TpJobmstr(String id){
		super(id);
	}
	
	@NotBlank(message="工单号码不能为空")
	@Length(min=0, max=18, message="工单号码长度不能超过 18 个字符")
	public String getFpJobno() {
		return fpJobno;
	}

	public void setFpJobno(String fpJobno) {
		this.fpJobno = fpJobno;
	}
	
	@NotBlank(message="产品系列不能为空")
	@Length(min=0, max=20, message="产品系列长度不能超过 20 个字符")
	public String getFpSeries() {
		return fpSeries;
	}

	public void setFpSeries(String fpSeries) {
		this.fpSeries = fpSeries;
	}
	
	@NotBlank(message="产品型号不能为空")
	@Length(min=0, max=25, message="产品型号长度不能超过 25 个字符")
	public String getFpModel() {
		return fpModel;
	}

	public void setFpModel(String fpModel) {
		this.fpModel = fpModel;
	}
	
	@Length(min=0, max=10, message="销售地区/国家长度不能超过 10 个字符")
	public String getFpDelcnty() {
		return fpDelcnty;
	}

	public void setFpDelcnty(String fpDelcnty) {
		this.fpDelcnty = fpDelcnty;
	}
	
	@NotNull(message="计划产量不能为空")
	public Integer getFpTarqty() {
		return fpTarqty;
	}

	public void setFpTarqty(Integer fpTarqty) {
		this.fpTarqty = fpTarqty;
	}
	
	@NotBlank(message="站位顺序检查不能为空")
	@Length(min=0, max=1, message="站位顺序检查长度不能超过 1 个字符")
	public String getFpChkProdSeq() {
		return fpChkProdSeq;
	}

	public void setFpChkProdSeq(String fpChkProdSeq) {
		this.fpChkProdSeq = fpChkProdSeq;
	}
	
	@Length(min=0, max=50, message="客户长度不能超过 50 个字符")
	public String getFpCust() {
		return fpCust;
	}

	public void setFpCust(String fpCust) {
		this.fpCust = fpCust;
	}
	
	@Length(min=0, max=25, message="牌子长度不能超过 25 个字符")
	public String getFpBrand() {
		return fpBrand;
	}

	public void setFpBrand(String fpBrand) {
		this.fpBrand = fpBrand;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getFpFinishDt() {
		return fpFinishDt;
	}

	public void setFpFinishDt(Date fpFinishDt) {
		this.fpFinishDt = fpFinishDt;
	}
	
	@Length(min=0, max=2, message="fp_plant长度不能超过 2 个字符")
	public String getFpPlant() {
		return fpPlant;
	}

	public void setFpPlant(String fpPlant) {
		this.fpPlant = fpPlant;
	}
	
	@Length(min=0, max=18, message="fp_mfgjob长度不能超过 18 个字符")
	public String getFpMfgjob() {
		return fpMfgjob;
	}

	public void setFpMfgjob(String fpMfgjob) {
		this.fpMfgjob = fpMfgjob;
	}
	
	@Length(min=0, max=100, message="fp_attach长度不能超过 100 个字符")
	public String getFpAttach() {
		return fpAttach;
	}

	public void setFpAttach(String fpAttach) {
		this.fpAttach = fpAttach;
	}
	
	@Length(min=0, max=255, message="fp_keypart长度不能超过 255 个字符")
	public String getFpKeypart() {
		return fpKeypart;
	}

	public void setFpKeypart(String fpKeypart) {
		this.fpKeypart = fpKeypart;
	}
	
	@Length(min=0, max=20, message="fp_pcb_no长度不能超过 20 个字符")
	public String getFpPcbNo() {
		return fpPcbNo;
	}

	public void setFpPcbNo(String fpPcbNo) {
		this.fpPcbNo = fpPcbNo;
	}
	
	@Length(min=0, max=30, message="fp_pcb_prg长度不能超过 30 个字符")
	public String getFpPcbPrg() {
		return fpPcbPrg;
	}

	public void setFpPcbPrg(String fpPcbPrg) {
		this.fpPcbPrg = fpPcbPrg;
	}
	
	@Length(min=0, max=1, message="fp_pcb_side长度不能超过 1 个字符")
	public String getFpPcbSide() {
		return fpPcbSide;
	}

	public void setFpPcbSide(String fpPcbSide) {
		this.fpPcbSide = fpPcbSide;
	}
	
	@Length(min=0, max=3, message="fp_pcb_type长度不能超过 3 个字符")
	public String getFpPcbType() {
		return fpPcbType;
	}

	public void setFpPcbType(String fpPcbType) {
		this.fpPcbType = fpPcbType;
	}
	
	public Integer getFpComponent() {
		return fpComponent;
	}

	public void setFpComponent(Integer fpComponent) {
		this.fpComponent = fpComponent;
	}
	
	public Integer getFpCompMulti() {
		return fpCompMulti;
	}

	public void setFpCompMulti(Integer fpCompMulti) {
		this.fpCompMulti = fpCompMulti;
	}
	
	@Length(min=0, max=1, message="fp_chk_type长度不能超过 1 个字符")
	public String getFpChkType() {
		return fpChkType;
	}

	public void setFpChkType(String fpChkType) {
		this.fpChkType = fpChkType;
	}
	
	@Length(min=0, max=18, message="fp_main_jobno长度不能超过 18 个字符")
	public String getFpMainJobno() {
		return fpMainJobno;
	}

	public void setFpMainJobno(String fpMainJobno) {
		this.fpMainJobno = fpMainJobno;
	}
	
	@Length(min=0, max=255, message="fp_firm_ver长度不能超过 255 个字符")
	public String getFpFirmVer() {
		return fpFirmVer;
	}

	public void setFpFirmVer(String fpFirmVer) {
		this.fpFirmVer = fpFirmVer;
	}
	
	public Long getFpRefreshA() {
		return fpRefreshA;
	}

	public void setFpRefreshA(Long fpRefreshA) {
		this.fpRefreshA = fpRefreshA;
	}
	
	public Long getFpRefreshB() {
		return fpRefreshB;
	}

	public void setFpRefreshB(Long fpRefreshB) {
		this.fpRefreshB = fpRefreshB;
	}
	
	@Length(min=0, max=28, message="fp_model_ver长度不能超过 28 个字符")
	public String getFpModelVer() {
		return fpModelVer;
	}

	public void setFpModelVer(String fpModelVer) {
		this.fpModelVer = fpModelVer;
	}
	
	public Integer getFpMiQty() {
		return fpMiQty;
	}

	public void setFpMiQty(Integer fpMiQty) {
		this.fpMiQty = fpMiQty;
	}
	
	public Integer getFpRd2shipQty() {
		return fpRd2shipQty;
	}

	public void setFpRd2shipQty(Integer fpRd2shipQty) {
		this.fpRd2shipQty = fpRd2shipQty;
	}
	
	public Integer getFpMlQty() {
		return fpMlQty;
	}

	public void setFpMlQty(Integer fpMlQty) {
		this.fpMlQty = fpMlQty;
	}
	
	@Length(min=0, max=200, message="fp_ml_rmk长度不能超过 200 个字符")
	public String getFpMlRmk() {
		return fpMlRmk;
	}

	public void setFpMlRmk(String fpMlRmk) {
		this.fpMlRmk = fpMlRmk;
	}
	
	@Length(min=0, max=3, message="-＞f26_rev_no长度不能超过 3 个字符")
	public String getFpRevNo() {
		return fpRevNo;
	}

	public void setFpRevNo(String fpRevNo) {
		this.fpRevNo = fpRevNo;
	}
	
	@Length(min=0, max=30, message="fp_remark长度不能超过 30 个字符")
	public String getFpRemark() {
		return fpRemark;
	}

	public void setFpRemark(String fpRemark) {
		this.fpRemark = fpRemark;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getFpLastModifyDt() {
		return fpLastModifyDt;
	}

	public void setFpLastModifyDt(Date fpLastModifyDt) {
		this.fpLastModifyDt = fpLastModifyDt;
	}
	
	@Length(min=0, max=1, message="Y-其他分廠生產N-本廠生產长度不能超过 1 个字符")
	public String getFpOtherMfg() {
		return fpOtherMfg;
	}

	public void setFpOtherMfg(String fpOtherMfg) {
		this.fpOtherMfg = fpOtherMfg;
	}
	
	@Length(min=0, max=25, message="fp_cust_model长度不能超过 25 个字符")
	public String getFpCustModel() {
		return fpCustModel;
	}

	public void setFpCustModel(String fpCustModel) {
		this.fpCustModel = fpCustModel;
	}
	
	public String getFpShipOutstd() {
		return fpShipOutstd;
	}

	public void setFpShipOutstd(String fpShipOutstd) {
		this.fpShipOutstd = fpShipOutstd;
	}
	
	public Double getFpExpectEfficiency() {
		return fpExpectEfficiency;
	}

	public void setFpExpectEfficiency(Double fpExpectEfficiency) {
		this.fpExpectEfficiency = fpExpectEfficiency;
	}
	
	@Length(min=0, max=1, message="fp_pp_flag长度不能超过 1 个字符")
	public String getFpPpFlag() {
		return fpPpFlag;
	}

	public void setFpPpFlag(String fpPpFlag) {
		this.fpPpFlag = fpPpFlag;
	}
	
	public String getFpDevInfo() {
		return fpDevInfo;
	}

	public void setFpDevInfo(String fpDevInfo) {
		this.fpDevInfo = fpDevInfo;
	}
	
	@Length(min=0, max=25, message="fp_cfg_processor长度不能超过 25 个字符")
	public String getFpCfgProcessor() {
		return fpCfgProcessor;
	}

	public void setFpCfgProcessor(String fpCfgProcessor) {
		this.fpCfgProcessor = fpCfgProcessor;
	}
	
	@Length(min=0, max=25, message="fp_cfg_os长度不能超过 25 个字符")
	public String getFpCfgOs() {
		return fpCfgOs;
	}

	public void setFpCfgOs(String fpCfgOs) {
		this.fpCfgOs = fpCfgOs;
	}
	
	@Length(min=0, max=25, message="fp_cfg_display长度不能超过 25 个字符")
	public String getFpCfgDisplay() {
		return fpCfgDisplay;
	}

	public void setFpCfgDisplay(String fpCfgDisplay) {
		this.fpCfgDisplay = fpCfgDisplay;
	}
	
	@Length(min=0, max=30, message="fp_cfg_storage长度不能超过 30 个字符")
	public String getFpCfgStorage() {
		return fpCfgStorage;
	}

	public void setFpCfgStorage(String fpCfgStorage) {
		this.fpCfgStorage = fpCfgStorage;
	}
	
	@Length(min=0, max=20, message="fp_cfg_ram长度不能超过 20 个字符")
	public String getFpCfgRam() {
		return fpCfgRam;
	}

	public void setFpCfgRam(String fpCfgRam) {
		this.fpCfgRam = fpCfgRam;
	}
	
	@Length(min=0, max=25, message="fp_cfg_color长度不能超过 25 个字符")
	public String getFpCfgColor() {
		return fpCfgColor;
	}

	public void setFpCfgColor(String fpCfgColor) {
		this.fpCfgColor = fpCfgColor;
	}
	
	@Length(min=0, max=14, message="fp_cfg_part长度不能超过 14 个字符")
	public String getFpCfgPart() {
		return fpCfgPart;
	}

	public void setFpCfgPart(String fpCfgPart) {
		this.fpCfgPart = fpCfgPart;
	}
	
	@Length(min=0, max=15, message="fp_cfg_ean长度不能超过 15 个字符")
	public String getFpCfgEan() {
		return fpCfgEan;
	}

	public void setFpCfgEan(String fpCfgEan) {
		this.fpCfgEan = fpCfgEan;
	}
	
	@Length(min=0, max=20, message="fp_cfg_cust_model长度不能超过 20 个字符")
	public String getFpCfgCustModel() {
		return fpCfgCustModel;
	}

	public void setFpCfgCustModel(String fpCfgCustModel) {
		this.fpCfgCustModel = fpCfgCustModel;
	}
	
	@Length(min=0, max=40, message="fp_cfg_desc长度不能超过 40 个字符")
	public String getFpCfgDesc() {
		return fpCfgDesc;
	}

	public void setFpCfgDesc(String fpCfgDesc) {
		this.fpCfgDesc = fpCfgDesc;
	}
	
	@Length(min=0, max=30, message="fp_cfg_wlan长度不能超过 30 个字符")
	public String getFpCfgWlan() {
		return fpCfgWlan;
	}

	public void setFpCfgWlan(String fpCfgWlan) {
		this.fpCfgWlan = fpCfgWlan;
	}
	
	public String getFpCfgCamera() {
		return fpCfgCamera;
	}

	public void setFpCfgCamera(String fpCfgCamera) {
		this.fpCfgCamera = fpCfgCamera;
	}
	
	@Length(min=0, max=5, message="fp_cfg_case_pack长度不能超过 5 个字符")
	public String getFpCfgCasePack() {
		return fpCfgCasePack;
	}

	public void setFpCfgCasePack(String fpCfgCasePack) {
		this.fpCfgCasePack = fpCfgCasePack;
	}
	
	@Length(min=0, max=10, message="fp_cfg_gw长度不能超过 10 个字符")
	public String getFpCfgGw() {
		return fpCfgGw;
	}

	public void setFpCfgGw(String fpCfgGw) {
		this.fpCfgGw = fpCfgGw;
	}
	
	@Length(min=0, max=10, message="fp_cfg_nw长度不能超过 10 个字符")
	public String getFpCfgNw() {
		return fpCfgNw;
	}

	public void setFpCfgNw(String fpCfgNw) {
		this.fpCfgNw = fpCfgNw;
	}
	
	@Length(min=0, max=20, message="fp_cfg_ship_prod长度不能超过 20 个字符")
	public String getFpCfgShipProd() {
		return fpCfgShipProd;
	}

	public void setFpCfgShipProd(String fpCfgShipProd) {
		this.fpCfgShipProd = fpCfgShipProd;
	}
	
	@Length(min=0, max=30, message="fp_cfg_storage2长度不能超过 30 个字符")
	public String getFpCfgStorage2() {
		return fpCfgStorage2;
	}

	public void setFpCfgStorage2(String fpCfgStorage2) {
		this.fpCfgStorage2 = fpCfgStorage2;
	}
	
	public String getFpCfgWarranty() {
		return fpCfgWarranty;
	}

	public void setFpCfgWarranty(String fpCfgWarranty) {
		this.fpCfgWarranty = fpCfgWarranty;
	}
	
	public String getFpCfgKb() {
		return fpCfgKb;
	}

	public void setFpCfgKb(String fpCfgKb) {
		this.fpCfgKb = fpCfgKb;
	}
	
	public String getFpCfgFingerprint() {
		return fpCfgFingerprint;
	}

	public void setFpCfgFingerprint(String fpCfgFingerprint) {
		this.fpCfgFingerprint = fpCfgFingerprint;
	}
	
	public String getFpCfgMarketingname() {
		return fpCfgMarketingname;
	}

	public void setFpCfgMarketingname(String fpCfgMarketingname) {
		this.fpCfgMarketingname = fpCfgMarketingname;
	}
	
	@Length(min=0, max=50, message="fp_cfg_cust_model_ship长度不能超过 50 个字符")
	public String getFpCfgCustModelShip() {
		return fpCfgCustModelShip;
	}

	public void setFpCfgCustModelShip(String fpCfgCustModelShip) {
		this.fpCfgCustModelShip = fpCfgCustModelShip;
	}
	
	@Length(min=0, max=18, message="fp_workflow_template长度不能超过 18 个字符")
	public String getFpWorkflowTemplate() {
		return fpWorkflowTemplate;
	}

	public void setFpWorkflowTemplate(String fpWorkflowTemplate) {
		this.fpWorkflowTemplate = fpWorkflowTemplate;
	}
	
	@Length(min=0, max=50, message="fp_cfg_upc长度不能超过 50 个字符")
	public String getFpCfgUpc() {
		return fpCfgUpc;
	}

	public void setFpCfgUpc(String fpCfgUpc) {
		this.fpCfgUpc = fpCfgUpc;
	}
	
	@Length(min=0, max=50, message="fp_cfg_lte长度不能超过 50 个字符")
	public String getFpCfgLte() {
		return fpCfgLte;
	}

	public void setFpCfgLte(String fpCfgLte) {
		this.fpCfgLte = fpCfgLte;
	}
	
	@Length(min=0, max=20, message="fp_resolution长度不能超过 20 个字符")
	public String getFpResolution() {
		return fpResolution;
	}

	public void setFpResolution(String fpResolution) {
		this.fpResolution = fpResolution;
	}
	
	@Length(min=0, max=1, message="fp_cfg_wwan长度不能超过 1 个字符")
	public String getFpCfgWwan() {
		return fpCfgWwan;
	}

	public void setFpCfgWwan(String fpCfgWwan) {
		this.fpCfgWwan = fpCfgWwan;
	}
	
	@Length(min=0, max=50, message="fp_cfg_warrantcty长度不能超过 50 个字符")
	public String getFpCfgWarrantcty() {
		return fpCfgWarrantcty;
	}

	public void setFpCfgWarrantcty(String fpCfgWarrantcty) {
		this.fpCfgWarrantcty = fpCfgWarrantcty;
	}
	
	@Length(min=0, max=25, message="fp_prod_bom长度不能超过 25 个字符")
	public String getFpProdBom() {
		return fpProdBom;
	}

	public void setFpProdBom(String fpProdBom) {
		this.fpProdBom = fpProdBom;
	}
	
	@Length(min=0, max=15, message="fp_cfg_winkeyitemid长度不能超过 15 个字符")
	public String getFpCfgWinkeyitemid() {
		return fpCfgWinkeyitemid;
	}

	public void setFpCfgWinkeyitemid(String fpCfgWinkeyitemid) {
		this.fpCfgWinkeyitemid = fpCfgWinkeyitemid;
	}
	
	@Length(min=0, max=15, message="fp_customer_po长度不能超过 15 个字符")
	public String getFpCustomerPo() {
		return fpCustomerPo;
	}

	public void setFpCustomerPo(String fpCustomerPo) {
		this.fpCustomerPo = fpCustomerPo;
	}
	
	@Length(min=0, max=10, message="fp_cfg_accessory长度不能超过 10 个字符")
	public String getFpCfgAccessory() {
		return fpCfgAccessory;
	}

	public void setFpCfgAccessory(String fpCfgAccessory) {
		this.fpCfgAccessory = fpCfgAccessory;
	}
	
}