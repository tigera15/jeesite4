/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.tp.service;

import com.freestyle.pms.modules.t9m.dao.T9mWorkflowMasterDao;
import com.freestyle.pms.modules.t9m.entity.T9mWorkflowMaster;
import com.freestyle.pms.modules.tg.entity.TgSeries;
import com.freestyle.pms.modules.tg.entity.ThModel;
import com.freestyle.pms.modules.tg.service.TgSeriesService;
import com.freestyle.pms.modules.tp.dao.TpJobmstrDao;
import com.freestyle.pms.modules.tp.entity.TpJobmstr;
import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.common.services.FileUploadUtil;
import com.jeesite.modules.file.entity.FileUpload;
import com.jeesite.modules.file.utils.FileUploadUtils;
import com.jeesite.modules.filemanager.dao.MyFileUploadDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * tp_jobmstrService
 * @author rocklee
 * @version 2019-07-09
 */
@Service
@Transactional(readOnly=true)
public class TpJobmstrService extends CrudService<TpJobmstrDao, TpJobmstr> {
	@Resource
	protected MyFileUploadDao mFileUploadDao;
	@Resource
	protected FileUploadUtil mFileUploadUtil;
	@Resource
	protected TgSeriesService tgSeriesService;
	@Resource
	protected T9mWorkflowMasterDao t9mWorkflowMasterDao;

	/**
	 * 获取单条数据
	 * @param tpJobmstr
	 * @return
	 */
	@Override
	public TpJobmstr get(TpJobmstr tpJobmstr) {
		return super.get(tpJobmstr);
	}
	
	/**
	 * 查询分页数据
	 * @param tpJobmstr 查询条件
	 * @return
	 */
	@Override
	public Page<TpJobmstr> findPage(TpJobmstr tpJobmstr) {
		tpJobmstr.getSqlMap().getOrder().setOrderBy("fp_jobno");
		return super.findPage(tpJobmstr);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param tpJobmstr
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(TpJobmstr tpJobmstr) {
		tpJobmstr.setFpMfgjob(tpJobmstr.getFpJobno());
		tpJobmstr.setFpMainJobno(tpJobmstr.getFpJobno());
		/*TpJobmstr lvRec=new TpJobmstr();
		lvRec.setFpJobno(tpJobmstr.getFpJobno());
		lvRec.setStatus(null);
		lvRec.getSqlMap().getWhere().remove("status");
		lvRec.getSqlMap().getWhere()
						.and("id", QueryType.NE,tpJobmstr.getId());
		lvRec=dao.getByEntity(lvRec);
		if (lvRec!=null&&!lvRec.getId().equals(tpJobmstr.getId())){
			throw new RuntimeException(Global.getText("MSG11101",tpJobmstr.getFpJobno()));
		}*/

		//check series, model
		TgSeries lvSeries=new TgSeries();
		lvSeries.setFgSeries(tpJobmstr.getFpSeries());
		lvSeries.setStatus(TgSeries.STATUS_NORMAL);
		lvSeries=tgSeriesService.get(lvSeries,false);
		if (lvSeries==null){
			throw new RuntimeException(Global.getText("MSG11102"));
		}
		ThModel lvModel=new ThModel(lvSeries);
		lvModel.setFhModel(tpJobmstr.getFpModel());
		lvModel.setStatus(ThModel.STATUS_NORMAL);
		//lvModel.getSqlMap().getWhere().and("fh_model",QueryType.EQ,tpJobmstr.getFpModel());
		if (tgSeriesService.getChild(lvModel)==null){
			throw new RuntimeException(Global.getText("MSG11102"));
		}
		if (!tpJobmstr.getFpWorkflowTemplate().isEmpty()){ //验证生产流程模板
			T9mWorkflowMaster lvWhere=new T9mWorkflowMaster();
			lvWhere.setF9mWorkflow(tpJobmstr.getFpWorkflowTemplate());
			lvWhere.setStatus("0");
			T9mWorkflowMaster lvT9m= t9mWorkflowMasterDao.getByEntity(lvWhere);
			if (lvT9m==null){
				throw new RuntimeException(Global.getText("MSG11103"));
			}
		}

		super.save(tpJobmstr);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(tpJobmstr.getId(), "tpJobmstr_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(tpJobmstr.getId(), "tpJobmstr_file");
		FileUpload lvWhere=new FileUpload();
		lvWhere.setBizKey(tpJobmstr.getId());
		lvWhere.setStatus(FileUpload.STATUS_DELETE);
		lvWhere.getSqlMap().getWhere().disableAutoAddStatusWhere();
		for (FileUpload item:mFileUploadDao.findList(lvWhere)){
			if (item.getStatus().equals(FileUpload.STATUS_DELETE)) {
				mFileUploadUtil.physicalDeleteFileEntity(item);
			}
		}
		mFileUploadUtil.cleanUp(); //清除垃圾文件,应该放在cronjob里面跑
	}
	
	/**
	 * 更新状态
	 * @param tpJobmstr
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(TpJobmstr tpJobmstr) {
		super.updateStatus(tpJobmstr);
	}
	
	/**
	 * 删除数据
	 * @param tpJobmstr
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(TpJobmstr tpJobmstr) {
		super.delete(tpJobmstr);
		for (FileUpload item:FileUploadUtils.findFileUpload(tpJobmstr.getId(),"tpJobmstr_file")){
			mFileUploadUtil.physicalDeleteFileEntity(item);
		}
		for (FileUpload item:FileUploadUtils.findFileUpload(tpJobmstr.getId(),"tpJobmstr_image")){
			mFileUploadUtil.physicalDeleteFileEntity(item);
		}
	}
	
}