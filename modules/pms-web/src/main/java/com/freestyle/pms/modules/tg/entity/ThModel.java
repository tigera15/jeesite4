/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.tg.entity;

import com.jeesite.common.entity.MyDataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 产品系列Entity
 * @author rocklee
 * @version 2019-07-01
 */
@Table(name="th_model", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="fh_tg_id", attrName="fhTgId.id", label="tg的id",isQuery =true),
		@Column(name="fh_model", attrName="fhModel", label="产品型号",queryType = QueryType.GTE),
		@Column(name="fh_desc", attrName="fhDesc", label="型号说明"),
		@Column(name="fh_photo", attrName="fhPhoto", label="图片", isQuery=false),
		@Column(name="fh_category", attrName="fhCategory", label="类型"),
		@Column(name="fh_cust_model", attrName="fhCustModel", label="客户型号"),
		@Column(name="fh_ref_flag", attrName="fhRefFlag", label="参考标志", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="update_by", attrName="updateBy", label="更新人", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false),
		@Column(name="status", attrName="status", label="状态", isQuery=false),
		@Column(name="fh_mdl_os_ver", attrName="fhMdlOsVer", label="fh_mdl_os_ver", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_mdl_bios_ver", attrName="fhMdlBiosVer", label="fh_mdl_bios_ver", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_mdl_ec_ver", attrName="fhMdlEcVer", label="fh_mdl_ec_ver", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_mdl_touchpad_ver", attrName="fhMdlTouchpadVer", label="fh_mdl_touchpad_ver", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_mdl_cpu", attrName="fhMdlCpu", label="fh_mdl_cpu", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_mdl_ram", attrName="fhMdlRam", label="fh_mdl_ram", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_mdl_ssd", attrName="fhMdlSsd", label="fh_mdl_ssd", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_mdl_runin_time", attrName="fhMdlRuninTime", label="fh_mdl_runin_time", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_mdl_deviceinfo", attrName="fhMdlDeviceinfo", label="fh_mdl_deviceinfo", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_mdl_other", attrName="fhMdlOther", label="fh_mdl_other", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_status", attrName="fhStatus", label="fh_status", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_serial", attrName="fhSerial", label="fh_serial", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_price", attrName="fhPrice", label="fh_price", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fh_genericname", attrName="fhGenericname", label="fh_genericname", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="create_by", attrName="createBy", label="create_by", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="create_date", isUpdate=false, isQuery=false),
	}, orderBy="a.create_date ASC"
)
public class ThModel extends MyDataEntity<ThModel> {
	
	private static final long serialVersionUID = 1L;
	private TgSeries fhTgId;		// tg的id 父类
	private String fhModel;		// 产品型号
	private String fhDesc;		// 型号说明
	private String fhPhoto;		// 图片
	private String fhCategory;		// 类型
	private String fhCustModel;		// 客户型号
	private String fhRefFlag;		// 参考标志
	private String fhMdlOsVer;		// fh_mdl_os_ver
	private String fhMdlBiosVer;		// fh_mdl_bios_ver
	private String fhMdlEcVer;		// fh_mdl_ec_ver
	private String fhMdlTouchpadVer;		// fh_mdl_touchpad_ver
	private String fhMdlCpu;		// fh_mdl_cpu
	private String fhMdlRam;		// fh_mdl_ram
	private String fhMdlSsd;		// fh_mdl_ssd
	private String fhMdlRuninTime;		// fh_mdl_runin_time
	private String fhMdlDeviceinfo;		// fh_mdl_deviceinfo
	private String fhMdlOther;		// fh_mdl_other
	private String fhStatus;		// fh_status
	private Long fhSerial;		// fh_serial
	private String fhPrice;		// fh_price
	private String fhGenericname;		// fh_genericname
	
	public ThModel() {
		this(null);
	}


	public ThModel(TgSeries fhTgId){
		this.fhTgId = fhTgId;
	}
	
	@Length(min=0, max=64, message="tg的id长度不能超过 64 个字符")
	public TgSeries getFhTgId() {
		return fhTgId;
	}

	public void setFhTgId(TgSeries fhTgId) {
		this.fhTgId = fhTgId;
	}
	
	@NotBlank(message="产品型号不能为空")
	@Length(min=0, max=25, message="产品型号长度不能超过 25 个字符")
	public String getFhModel() {
		return fhModel;
	}

	public void setFhModel(String fhModel) {
		this.fhModel = fhModel;
	}
	
	@Length(min=0, max=40, message="型号说明长度不能超过 40 个字符")
	public String getFhDesc() {
		return fhDesc;
	}

	public void setFhDesc(String fhDesc) {
		this.fhDesc = fhDesc;
	}
	
	@Length(min=0, max=100, message="图片长度不能超过 100 个字符")
	public String getFhPhoto() {
		return fhPhoto;
	}

	public void setFhPhoto(String fhPhoto) {
		this.fhPhoto = fhPhoto;
	}
	
	@NotBlank(message="类型不能为空")
	@Length(min=0, max=10, message="类型长度不能超过 10 个字符")
	public String getFhCategory() {
		return fhCategory;
	}

	public void setFhCategory(String fhCategory) {
		this.fhCategory = fhCategory;
	}
	
	@NotBlank(message="客户型号不能为空")
	@Length(min=0, max=25, message="客户型号长度不能超过 25 个字符")
	public String getFhCustModel() {
		return fhCustModel;
	}

	public void setFhCustModel(String fhCustModel) {
		this.fhCustModel = fhCustModel;
	}
	
	@Length(min=0, max=1, message="参考标志长度不能超过 1 个字符")
	public String getFhRefFlag() {
		return fhRefFlag;
	}

	public void setFhRefFlag(String fhRefFlag) {
		this.fhRefFlag = fhRefFlag;
	}
	
	@Length(min=0, max=60, message="fh_mdl_os_ver长度不能超过 60 个字符")
	public String getFhMdlOsVer() {
		return fhMdlOsVer;
	}

	public void setFhMdlOsVer(String fhMdlOsVer) {
		this.fhMdlOsVer = fhMdlOsVer;
	}
	
	@Length(min=0, max=60, message="fh_mdl_bios_ver长度不能超过 60 个字符")
	public String getFhMdlBiosVer() {
		return fhMdlBiosVer;
	}

	public void setFhMdlBiosVer(String fhMdlBiosVer) {
		this.fhMdlBiosVer = fhMdlBiosVer;
	}
	
	@Length(min=0, max=60, message="fh_mdl_ec_ver长度不能超过 60 个字符")
	public String getFhMdlEcVer() {
		return fhMdlEcVer;
	}

	public void setFhMdlEcVer(String fhMdlEcVer) {
		this.fhMdlEcVer = fhMdlEcVer;
	}
	
	@Length(min=0, max=60, message="fh_mdl_touchpad_ver长度不能超过 60 个字符")
	public String getFhMdlTouchpadVer() {
		return fhMdlTouchpadVer;
	}

	public void setFhMdlTouchpadVer(String fhMdlTouchpadVer) {
		this.fhMdlTouchpadVer = fhMdlTouchpadVer;
	}
	
	@Length(min=0, max=30, message="fh_mdl_cpu长度不能超过 30 个字符")
	public String getFhMdlCpu() {
		return fhMdlCpu;
	}

	public void setFhMdlCpu(String fhMdlCpu) {
		this.fhMdlCpu = fhMdlCpu;
	}
	
	@Length(min=0, max=30, message="fh_mdl_ram长度不能超过 30 个字符")
	public String getFhMdlRam() {
		return fhMdlRam;
	}

	public void setFhMdlRam(String fhMdlRam) {
		this.fhMdlRam = fhMdlRam;
	}
	
	@Length(min=0, max=30, message="fh_mdl_ssd长度不能超过 30 个字符")
	public String getFhMdlSsd() {
		return fhMdlSsd;
	}

	public void setFhMdlSsd(String fhMdlSsd) {
		this.fhMdlSsd = fhMdlSsd;
	}
	
	@Length(min=0, max=60, message="fh_mdl_runin_time长度不能超过 60 个字符")
	public String getFhMdlRuninTime() {
		return fhMdlRuninTime;
	}

	public void setFhMdlRuninTime(String fhMdlRuninTime) {
		this.fhMdlRuninTime = fhMdlRuninTime;
	}
	
	@Length(min=0, max=1024, message="fh_mdl_deviceinfo长度不能超过 1024 个字符")
	public String getFhMdlDeviceinfo() {
		return fhMdlDeviceinfo;
	}

	public void setFhMdlDeviceinfo(String fhMdlDeviceinfo) {
		this.fhMdlDeviceinfo = fhMdlDeviceinfo;
	}
	
	@Length(min=0, max=80, message="fh_mdl_other长度不能超过 80 个字符")
	public String getFhMdlOther() {
		return fhMdlOther;
	}

	public void setFhMdlOther(String fhMdlOther) {
		this.fhMdlOther = fhMdlOther;
	}
	
	@Length(min=0, max=1, message="fh_status长度不能超过 1 个字符")
	public String getFhStatus() {
		return fhStatus;
	}

	public void setFhStatus(String fhStatus) {
		this.fhStatus = fhStatus;
	}
	
	public Long getFhSerial() {
		return fhSerial;
	}

	public void setFhSerial(Long fhSerial) {
		this.fhSerial = fhSerial;
	}
	
	@Length(min=0, max=10, message="fh_price长度不能超过 10 个字符")
	public String getFhPrice() {
		return fhPrice;
	}

	public void setFhPrice(String fhPrice) {
		this.fhPrice = fhPrice;
	}
	
	@Length(min=0, max=10, message="fh_genericname长度不能超过 10 个字符")
	public String getFhGenericname() {
		return fhGenericname;
	}

	public void setFhGenericname(String fhGenericname) {
		this.fhGenericname = fhGenericname;
	}
	
}