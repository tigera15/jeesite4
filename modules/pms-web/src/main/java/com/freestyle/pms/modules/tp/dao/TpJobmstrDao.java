/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.tp.dao;

import com.freestyle.pms.modules.tp.entity.TpJobmstr;
import com.jeesite.common.dao.MyCrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;

/**
 * tp_jobmstrDAO接口
 * @author rocklee
 * @version 2019-07-09
 */
@MyBatisDao
public interface TpJobmstrDao extends MyCrudDao<TpJobmstr> {
	
}