/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.tg.web;

import com.freestyle.common.utils.Util;
import com.freestyle.pms.modules.tg.entity.TgSeries;
import com.freestyle.pms.modules.tg.entity.ThModel;
import com.freestyle.pms.modules.tg.service.TgSeriesService;
import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 产品系列Controller
 * @author rocklee
 * @version 2019-07-01
 */
@Controller
@RequestMapping(value = "${adminPath}/tg/tgSeries")
public class TgSeriesController extends BaseController {

	@Autowired
	private TgSeriesService tgSeriesService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute()
	public TgSeries get(String id, boolean isNewRecord,HttpServletRequest request, HttpServletResponse response) {
	  if (Util.fillNullStr(id).equals("")) return new TgSeries(); //避免一些无谓的引用, by 6/jul/2019
		TgSeries lvRet= tgSeriesService.get(id, isNewRecord);
		/*if (id!=null) {
			ThModel thModel = new ThModel(lvRet);
			thModel.setPage(new Page<ThModel>(request, response));
			Page<ThModel> lvPage = tgSeriesService.findChildPage(thModel);
			lvRet.setDetailPage(lvPage);
		}*/
		return lvRet;
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("tg:tgSeries:view")
	@RequestMapping(value = {"list", ""})
	public String list(TgSeries tgSeries, Model model) {
		model.addAttribute("tgSeries", tgSeries);
		return "modules/tg/tgSeriesList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("tg:tgSeries:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<TgSeries> listData(TgSeries tgSeries, HttpServletRequest request, HttpServletResponse response) {
		tgSeries.setPage(new Page<>(request, response));
		//tgSeries
		Page<TgSeries> page = tgSeriesService.findPage(tgSeries);
		return page;
	}
	/**
	 * 查询从表数据
	 * by rock 5/Jul/2019
	 */
	@RequiresPermissions("tg:tgSeries:view")
	@RequestMapping(value = "listChildData")
	@ResponseBody
	public Page<ThModel> listChildData(TgSeries tgSeries, ThModel thModel, HttpServletRequest request, HttpServletResponse response) {
		thModel.setId(null);
    thModel.setPage(new Page<>(request, response));
    if (Util.fillNullStr(tgSeries.getId()).equals("")&& Util.fillNullStr(tgSeries.getFgSeries()).equals("")){ //new record
    	tgSeries=new TgSeries();
			tgSeries.setId(".");
		}
    else if (!Util.fillNullStr(tgSeries.getFgSeries()).equals("")){
			tgSeries=tgSeriesService.get(tgSeries,false);
		}
    thModel.setFhTgId(tgSeries);
		Page<ThModel> page = tgSeriesService.findChildPage(thModel);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("tg:tgSeries:view")
	@RequestMapping(value = "form")
	public String form(TgSeries tgSeries, Model model) {
		model.addAttribute("tgSeries", tgSeries);
		return "modules/tg/tgSeriesForm";
	}

	/**
	 * 保存产品系列型号
	 */
	@RequiresPermissions("tg:tgSeries:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated TgSeries tgSeries) {
		tgSeriesService.save(tgSeries);
		return renderResult(Global.TRUE, text("保存产品系列型号成功！"));
	}
	
	/**
	 * 停用产品系列型号
	 */
	@RequiresPermissions("tg:tgSeries:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(TgSeries tgSeries) {
		tgSeries.setStatus(TgSeries.STATUS_DISABLE);
		tgSeriesService.updateStatus(tgSeries);
		return renderResult(Global.TRUE, text("停用产品系列型号成功"));
	}
	
	/**
	 * 启用产品系列型号
	 */
	@RequiresPermissions("tg:tgSeries:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(TgSeries tgSeries) {
		tgSeries.setStatus(TgSeries.STATUS_NORMAL);
		tgSeriesService.updateStatus(tgSeries);
		return renderResult(Global.TRUE, text("启用产品系列型号成功"));
	}

	/**
	 * 启用产品型号
	 */
	@RequiresPermissions("tg:tgSeries:edit")
	@RequestMapping(value = "enable-child")
	@ResponseBody
	public String enableChild(ThModel entity) {
		entity.setStatus(ThModel.STATUS_NORMAL);
		tgSeriesService.updateChildStatus(entity);
		return renderResult(Global.TRUE, text("启用产品型号成功"));
	}
	/**
	 * 停用产品型号
	 */
	@RequiresPermissions("tg:tgSeries:edit")
	@RequestMapping(value = "disable-child")
	@ResponseBody
	public String disable(ThModel entity) {
		entity.setStatus(ThModel.STATUS_DISABLE);
		tgSeriesService.updateChildStatus(entity);
		return renderResult(Global.TRUE, text("停用产品型号成功"));
	}

	
	/**
	 * 删除产品系列型号
	 */
	@RequiresPermissions("tg:tgSeries:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(TgSeries tgSeries) {
		tgSeriesService.delete(tgSeries);
		return renderResult(Global.TRUE, text("删除产品系列型号成功！"));
	}
	
}