/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.td.web;

import com.freestyle.pms.modules.td.entity.TdTest1;
import com.freestyle.pms.modules.td.service.TdTest1Service;
import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 单一表测试功能Controller
 * @author rocklee
 * @version 2019-06-27
 */
@Controller
@RequestMapping(value = "${adminPath}/td/tdTest1")
public class TdTest1Controller extends BaseController {

	@Autowired
	private TdTest1Service tdTest1Service;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public TdTest1 get(String id, boolean isNewRecord) {
		return tdTest1Service.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("td:tdTest1:view")
	@RequestMapping(value = {"list", ""})
	public String list(TdTest1 tdTest1, Model model) {
		model.addAttribute("tdTest1", tdTest1);
		return "modules/td/tdTest1List";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("td:tdTest1:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<TdTest1> listData(TdTest1 tdTest1, HttpServletRequest request, HttpServletResponse response) {
		logger.info(tdTest1.getSqlMap().getWhere().toSql());
		tdTest1.setPage(new Page<>(request, response));
		logger.info(tdTest1.getSqlMap().getWhere().toSql());
		Page<TdTest1> page = tdTest1Service.findPage(tdTest1);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("td:tdTest1:view")

	@RequestMapping(value = "form")
	public String form(TdTest1 tdTest1, Model model) {
		model.addAttribute("tdTest1", tdTest1);
		return "modules/td/tdTest1Form";
	}

	/**
	 * 保存单一表测试
	 */
	@RequiresPermissions("td:tdTest1:edit")
	@PostMapping(value = "save")
	@ResponseBody

	public String save(@Validated TdTest1 tdTest1) {
			tdTest1Service.save(tdTest1);
			return renderResult(Global.TRUE, text("保存单一表测试成功！"));
	}
	
	/**
	 * 停用单一表测试
	 */
	@RequiresPermissions("td:tdTest1:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(TdTest1 tdTest1) {
		tdTest1.setStatus(TdTest1.STATUS_DISABLE);
		tdTest1Service.updateStatus(tdTest1);
		return renderResult(Global.TRUE, text("停用单一表测试成功"));
	}
	
	/**
	 * 启用单一表测试
	 */
	@RequiresPermissions("td:tdTest1:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(TdTest1 tdTest1) {
		tdTest1.setStatus(TdTest1.STATUS_NORMAL);
		tdTest1Service.updateStatus(tdTest1);
		return renderResult(Global.TRUE, text("启用单一表测试成功"));
	}
	
	/**
	 * 删除单一表测试
	 */
	@RequiresPermissions("td:tdTest1:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(TdTest1 tdTest1) {
		tdTest1Service.delete(tdTest1);
		return renderResult(Global.TRUE, text("删除单一表测试成功！"));
	}
	
}