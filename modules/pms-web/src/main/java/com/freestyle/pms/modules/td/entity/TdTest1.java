/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.td.entity;

import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 单一表测试功能Entity
 * @author rocklee
 * @version 2019-06-27
 */
@Table(name="td_test1", alias="a", columns={
		@Column(name="id", attrName="id", label="编号", isPK=true),
		@Column(name="fd_user", attrName="fdUser", label="用户名", queryType=QueryType.GTE),
		@Column(name="fd_name", attrName="fdName", label="姓名", queryType=QueryType.LIKE),
		@Column(name="fd_sex", attrName="fdSex", label="姓别"),
		@Column(name="status", attrName="status", label="状态", comment="状态（0正常 1删除 2停用）", isUpdate=false),
		@Column(name="create_by", attrName="createBy", label="创建人", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="创建时间", isUpdate=false, isQuery=false),
		@Column(name="update_by", attrName="updateBy", label="修改人", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="修改时间", isQuery=false),
	}, orderBy="a.update_date DESC"
)
public class TdTest1 extends DataEntity<TdTest1> {
	
	private static final long serialVersionUID = 1L;
	private String fdUser;		// 用户名
	private String fdName;		// 姓名
	private String fdSex;		// 姓别
	
	public TdTest1() {
		this(null);
	}

	public TdTest1(String id){
		super(id);
	}
	
	@NotBlank(message="用户名不能为空")
	@Length(min=0, max=10, message="用户名长度不能超过 10 个字符")
	public String getFdUser() {
		return fdUser;
	}

	public void setFdUser(String fdUser) {
		this.fdUser = fdUser;
	}
	
	@NotBlank(message="姓名不能为空")
	@Length(min=0, max=15, message="姓名长度不能超过 15 个字符")
	public String getFdName() {
		return fdName;
	}

	public void setFdName(String fdName) {
		this.fdName = fdName;
	}
	
	@NotBlank(message="姓别不能为空")
	@Length(min=0, max=1, message="姓别长度不能超过 1 个字符")
	public String getFdSex() {
		return fdSex;
	}

	public void setFdSex(String fdSex) {
		this.fdSex = fdSex;
	}
	
}