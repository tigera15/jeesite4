/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.freestyle.pms.modules.tg.entity;

import com.jeesite.common.collect.ListUtils;
import com.jeesite.common.entity.MyDataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 产品系列Entity
 * @author rocklee
 * @version 2019-07-01
 */
@Table(name="tg_series", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="fg_series", attrName="fgSeries", label="产品系列", queryType = QueryType.GTE),
		@Column(name="fg_desc", attrName="fgDesc", label="系列说明"),
		@Column(name="fg_form_factor", attrName="fgFormFactor", label="fg_form_factor", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="fg_status", attrName="fgStatus", label="fg_status", isInsert=false, isUpdate=false, isQuery=false),
		@Column(name="create_by", attrName="createBy", label="新建人", isUpdate=false, isQuery=false),
		@Column(name="create_date", attrName="createDate", label="新建时间", isUpdate=false, isQuery=false),
		@Column(name="update_by", attrName="updateBy", label="更新人", isQuery=false),
		@Column(name="update_date", attrName="updateDate", label="更新时间", isQuery=false),
		@Column(name="status", attrName="status", label="状态", isUpdate=false),
	}, orderBy="a.update_date DESC"
)
//还要解决准确设置的问题update_dt,update_by
public class TgSeries extends MyDataEntity<TgSeries> {
	
	private static final long serialVersionUID = 1L;
	private String fgSeries;		// 产品系列
	private String fgDesc;		// 系列说明
	private String fgFormFactor;		// fg_form_factor
	private String fgStatus;		// fg_status
	private List<ThModel> thModelList = ListUtils.newArrayList();		// 子表列表
	
	public TgSeries() {
		this(null);
	}

	public TgSeries(String id){
		super(id);
	}
	
	@NotBlank(message="产品系列不能为空")
	@Length(min=0, max=20, message="产品系列长度不能超过 20 个字符")
	public String getFgSeries() {
		return fgSeries;
	}

	public void setFgSeries(String fgSeries) {
		this.fgSeries = fgSeries;
	}
	
	@Length(min=0, max=40, message="系列说明长度不能超过 40 个字符")
	public String getFgDesc() {
		return fgDesc;
	}

	public void setFgDesc(String fgDesc) {
		this.fgDesc = fgDesc;
	}
	
	@Length(min=0, max=2, message="fg_form_factor长度不能超过 2 个字符")
	public String getFgFormFactor() {
		return fgFormFactor;
	}

	public void setFgFormFactor(String fgFormFactor) {
		this.fgFormFactor = fgFormFactor;
	}
	
	@Length(min=0, max=1, message="fg_status长度不能超过 1 个字符")
	public String getFgStatus() {
		return fgStatus;
	}

	public void setFgStatus(String fgStatus) {
		this.fgStatus = fgStatus;
	}
	
	public List<ThModel> getThModelList() {
		return thModelList;
	}

	public void setThModelList(List<ThModel> thModelList) {
		this.thModelList = thModelList;
	}


}