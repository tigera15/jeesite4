(function (app){
    app.setSeriesSelection=function(pvJQueryUI) {
        if (pvJQueryUI.data('select2')){
            pvJQueryUI.select2("destroy").empty();
        }
        pvJQueryUI.select2({
            ajax: {
                url: "../../tg/tgSeries/listData",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        fgSeries: params.term.toUpperCase().trim(), // search term
                        pageNo: params.page,
                        pageSize: 20,
                        orderBy: "fg_series"
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    var lvRet = [];
                    for (var i = 0; i < data.list.length; i++) {
                        lvRet.push({"id": data.list[i].fgSeries,"tid": data.list[i].id,"text": data.list[i].fgSeries});
                    }
                    return {
                        results: lvRet,
                        pagination: {
                            more: (params.page * 20) < data.count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1
        })
        .on("select2:close", function (e) {
            //console.log(e);
            if (typeof e.params.originalSelect2Event!="undefined"){
                pvJQueryUI.data("tid",e.params.originalSelect2Event.data.tid);
            }
        });
    },
    app.setModelSelection=function(pvJQueryUI,pvOnGetSeries) {
        if (pvJQueryUI.data('select2')){
            pvJQueryUI.select2("destroy").empty();
        }
        pvJQueryUI.select2({
            ajax: {
                url: "../../tg/tgSeries/listChildData",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        fhModel: params.term.toUpperCase().trim(), // search term
                        fgSeries:pvOnGetSeries,
                        pageNo: params.page,
                        pageSize: 20,
                        orderBy: "fh_model"
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    var lvRet = [];
                    for (var i = 0; i < data.list.length; i++) {
                        lvRet.push({"id": data.list[i].fhModel, "text": data.list[i].fhModel});
                    }
                    return {
                        results: lvRet,
                        pagination: {
                            more: (params.page * 20) < data.count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1
        });
    },
    app.setWorkFlowSelection=function(pvJQueryUI) {
        if (pvJQueryUI.data('select2')){
            pvJQueryUI.select2("destroy").empty();
        }
        pvJQueryUI.select2({
            ajax: {
                url: "../../t9m/t9mWorkflowMaster/listData-prefix",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        f9mWorkflow: params.term.toUpperCase().trim(), // search term
                        pageNo: params.page,
                        pageSize: 20,
                        orderBy: "f9m_workflow"
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    var lvRet = [];
                    for (var i = 0; i < data.list.length; i++) {
                        lvRet.push({"id": data.list[i].f9mWorkflow,"tid": data.list[i].id,"text": data.list[i].f9mWorkflow});
                    }
                    return {
                        results: lvRet,
                        pagination: {
                            more: (params.page * 20) < data.count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1
        })
            .on("select2:close", function (e) {
                //console.log(e);
                if (typeof e.params.originalSelect2Event!="undefined"){
                    pvJQueryUI.data("tid",e.params.originalSelect2Event.data.tid);
                }
            });
    }
}(window.app))